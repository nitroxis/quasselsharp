﻿using System;

namespace QuasselSharp
{
	/// <summary>
	/// Represents an attribute for methods that act as handler for RpcCall messages.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method, AllowMultiple=false)]
	public sealed class RpcCallAttribute : Attribute
	{
		#region Fields

		private readonly string name;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the name of the RpcCall method.
		/// </summary>
		public string Name
		{
			get { return this.name; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new RpcCallAttribute.
		/// </summary>
		public RpcCallAttribute(string name)
		{
			this.name = name;
		}

		#endregion

		#region Methods

		#endregion
	}
}
