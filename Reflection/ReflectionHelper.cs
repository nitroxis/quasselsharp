﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;

namespace QuasselSharp
{
	/// <summary>
	/// Reflection helper methods.
	/// </summary>
	public static class ReflectionHelper
	{
		#region Methods

		private static readonly Dictionary<Type, ReadOnlyDictionary<string, SyncHandler>> syncHandlerCache;
		
		static ReflectionHelper()
		{
			syncHandlerCache = new Dictionary<Type, ReadOnlyDictionary<string, SyncHandler>>();
		}

		/// <summary>
		/// Invokes a method.
		/// </summary>
		/// <param name="target"></param>
		/// <param name="method"></param>
		/// <param name="p"></param>
		public static void Invoke(object target, MethodInfo method, object[] p)
		{
			ParameterInfo[] parameters = method.GetParameters();
			object[] paramValues = new object[parameters.Length];

			// cast all values to the parameter types.
			for (int i = 0; i < parameters.Length; i++)
			{
				if (parameters[i].ParameterType.IsInstanceOfType(p[i]))
				{
					paramValues[i] = p[i];
				}
				else
				{
					if (parameters[i].ParameterType.IsEnum)
						paramValues[i] = Enum.ToObject(parameters[i].ParameterType, Convert.ChangeType(p[i], parameters[i].ParameterType.GetEnumUnderlyingType()));
					else
						paramValues[i] = Convert.ChangeType(p[i], parameters[i].ParameterType);
				}
			}
			
			// invoke method.
			method.Invoke(target, paramValues);
		}

		/// <summary>
		/// Gets all Sync message handlers of the specified object.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static ReadOnlyDictionary<string, SyncHandler> GetSyncHandlers(Type type)
		{
			if (type == null)
				throw new ArgumentNullException("type");

			ReadOnlyDictionary<string, SyncHandler> dict;
			if (syncHandlerCache.TryGetValue(type, out dict))
				return dict;
			
			MethodInfo[] methods = type.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
			Dictionary<string, SyncHandler> handlers = new Dictionary<string, SyncHandler>();

			foreach (MethodInfo method in methods)
			{
				SyncAttribute attr = method.GetCustomAttribute<SyncAttribute>();
				if (attr == null)
					continue;
				
				MethodInfo methodCopy = method;
				handlers.Add(attr.Name, (target, p) => Invoke(target, methodCopy, p));
			}

			dict = new ReadOnlyDictionary<string, SyncHandler>(handlers);
			syncHandlerCache.Add(type, dict);
			return dict;
		}

		#endregion
	}
}
