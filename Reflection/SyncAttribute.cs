﻿using System;

namespace QuasselSharp
{
	/// <summary>
	/// Represents an attribute for methods that act as handler for sync messages.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method, AllowMultiple=false)]
	public sealed class SyncAttribute : Attribute
	{
		#region Fields

		private readonly string name;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the name of the sync method.
		/// </summary>
		public string Name
		{
			get { return this.name; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new SyncAttribute.
		/// </summary>
		public SyncAttribute(string name)
		{
			this.name = name;
		}

		#endregion

		#region Methods

		#endregion
	}
}
