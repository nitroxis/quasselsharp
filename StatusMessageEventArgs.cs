﻿using System;

namespace QuasselSharp
{
	/// <summary>
	/// Status message event arguments.
	/// </summary>
	public sealed class StatusMessageEventArgs : EventArgs
	{
		#region Fields

		private readonly string message;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the message.
		/// </summary>
		public string Message
		{
			get { return this.message; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new StatusMessageEventArgs.
		/// </summary>
		public StatusMessageEventArgs(string message)
		{
			this.message = message;
		}

		#endregion
	}
}
