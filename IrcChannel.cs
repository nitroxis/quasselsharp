﻿using System;
using System.Collections.Generic;

namespace QuasselSharp
{
	/// <summary>
	/// Represents an IRC channel.
	/// </summary>
	public sealed class IrcChannel
	{
		#region Fields

		private readonly Network network;
		private readonly Dictionary<IrcUser, string> userModes;
		
		private string name;
		private string topic;
		private string password;
		private bool encrypted;

		private readonly Dictionary<char, List<string>> channelModesA;
		private readonly Dictionary<char, string> channelModesB;
		private readonly Dictionary<char, string> channelModesC;
		private readonly HashSet<char> channelModesD;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the name of the channel.
		/// </summary>
		public string Name
		{
			get { return this.name; }
			[Sync("setName")]
			set { this.name = value; }
		}

		/// <summary>
		/// Gets or sets the topic of the channel.
		/// </summary>
		public string Topic
		{
			get { return this.topic; }
			[Sync("setTopic")]
			set { this.topic = value; }
		}

		/// <summary>
		/// Gets or sets the password of the channel.
		/// </summary>
		public string Password
		{
			get { return this.password; }
			[Sync("setPassword")]
			set { this.password = value; }
		}

		/// <summary>
		/// Gets or sets a value that indicates whether the channel is encrypted.
		/// </summary>
		public bool Encrypted
		{
			get { return this.encrypted; }
			[Sync("setEncrypted")]
			set { this.encrypted = value; }
		}

		public Dictionary<char, List<string>> ChannelModesA
		{
			get { return this.channelModesA; }
		}

		public Dictionary<char, string> ChannelModesB
		{
			get { return this.channelModesB; }
		}

		public Dictionary<char, string> ChannelModesC
		{
			get { return this.channelModesC; }
		}

		public HashSet<char> ChannelModesD
		{
			get { return this.channelModesD; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new IrcChannel.
		/// </summary>
		public IrcChannel(Network network)
		{
			this.network = network;
			this.userModes = new Dictionary<IrcUser, string>(new IrcUser.EqualityComparer());
			this.channelModesA = new Dictionary<char, List<string>>();
			this.channelModesB = new Dictionary<char, string>();
			this.channelModesC = new Dictionary<char, string>();
			this.channelModesD = new HashSet<char>();
		}

		/// <summary>
		/// Creates a new IrcChannel.
		/// </summary>
		/// <param name="network"></param>
		/// <param name="name"></param>
		public IrcChannel(Network network, string name)
			: this(network)
		{
			this.name = name;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Joins the specified users.
		/// </summary>
		/// <param name="users"></param>
		/// <param name="modes"></param>
		public void JoinIrcUsers(IrcUser[] users, string[] modes)
		{
			for (int i = 0; i < users.Length; i++)
				this.userModes[users[i]] = modes[i];
		}

		[Sync("joinIrcUsers")]
		private void joinIrcUsers(string[] nicks, string[] modes)
		{
			IrcUser[] users = new IrcUser[nicks.Length];

			for (int i = 0; i < nicks.Length; i++)
			{
				users[i] = this.network.GetUserByNick(nicks[i]);
				if (users[i] == null)
					Console.Error.WriteLine("joinIrcUsers: Could not find user \"{0}\".", nicks[i]);
			}

			this.JoinIrcUsers(users, modes);
		}

		public void JoinIrcUser(IrcUser user)
		{
			if (this.userModes.ContainsKey(user))
				return;

			this.userModes.Add(user, "");
			user.JoinChannel(this);
		}

		/// <summary>
		/// Parts the specified user.
		/// </summary>
		/// <param name="user"></param>
		public void Part(IrcUser user)
		{
			if (!this.userModes.Remove(user))
				return;

			user.PartChannel(this);
		}

		[Sync("part")]
		private void part(string nick)
		{
			IrcUser user = this.network.GetUserByNick(nick);
			if (user == null)
			{
				Console.Error.WriteLine("part: Could not find user \"{0}\".", nick);
				return;
			}

			this.Part(user);
		}

		public void SetUserModes(IrcUser user, string modes)
		{
			this.userModes[user] = modes;
		}

		[Sync("setUserModes")]
		private void setUserModes(string nick, string modes)
		{
			IrcUser user = this.network.GetUserByNick(nick);
			if (user == null)
			{
				Console.Error.WriteLine("setUserModes: Could not find user \"{0}\".", nick);
				return;
			}

			this.SetUserModes(user, modes);
		}

		public void AddUserMode(IrcUser user, string mode)
		{
			if (!this.userModes.ContainsKey(user))
			{
				this.userModes.Add(user, mode);
			}
			else
			{
				if (this.userModes[user] == null)
					this.userModes[user] = mode;
				else if (this.userModes[user].IndexOf(mode, StringComparison.Ordinal) < 0)
					this.userModes[user] += mode;
			}
		}

		[Sync("addUserMode")]
		private void addUserMode(string nick, string mode)
		{
			IrcUser user = this.network.GetUserByNick(nick);
			if (user == null)
			{
				Console.Error.WriteLine("addUserMode: Could not find user \"{0}\".", nick);
				return;
			}

			this.AddUserMode(user, mode);
		}

		public void RemoveUserMode(IrcUser user, string mode)
		{
			string modes;
			if (!this.userModes.TryGetValue(user, out modes))
				return;

			int index = modes.IndexOf(mode, StringComparison.Ordinal);
			if (index < 0)
				return;
			this.userModes[user] = modes.Remove(index, mode.Length);
		}

		[Sync("removeUserMode")]
		private void removeUserMode(string nick, string mode)
		{
			IrcUser user = this.network.GetUserByNick(nick);
			if (user == null)
			{
				Console.Error.WriteLine("removeUserMode: Could not find user \"{0}\".", nick);
				return;
			}

			this.RemoveUserMode(user, mode);
		}

		[Sync("addChannelMode")]
		public void AddChannelMode(char mode, string value)
		{
			ChannelModeType modeType = this.network.GetChannelModeType(mode);

			if (modeType == ChannelModeType.A)
			{
				if (!this.channelModesA.ContainsKey(mode))
					this.channelModesA[mode] = new List<string>();

				if (!this.channelModesA[mode].Contains(value))
					this.channelModesA[mode].Add(value);
			}
			else if (modeType == ChannelModeType.B)
			{
				this.channelModesB[mode] = value;
			}
			else if (modeType == ChannelModeType.C)
			{
				this.channelModesC[mode] = value;
			}
			else
			{
				this.channelModesD.Add(mode);
			}
		}

		[Sync("removeChannelMode")]
		public void RemoveChannelMode(char mode, string value)
		{
			ChannelModeType modeType = this.network.GetChannelModeType(mode);

			if (modeType == ChannelModeType.A && this.channelModesA.ContainsKey(mode))
				this.channelModesA[mode].Remove(value);
			else if (modeType == ChannelModeType.B)
				this.channelModesB.Remove(mode);
			else if (modeType == ChannelModeType.C)
				this.channelModesC.Remove(mode);
			else
				this.channelModesD.Remove(mode);
		}

		public override string ToString()
		{
			return this.name;
		}

		#endregion
	}
}
