﻿using System;
using System.Collections.Generic;

namespace QuasselSharp.Qt
{
	/// <summary>
	/// Represents a wrapper for QVariantList.
	/// </summary>
	public sealed class QVariantList : List<object>
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new QVariantList.
		/// </summary>
		public QVariantList()
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Removes the first element from the list and returns the element.
		/// </summary>
		/// <returns></returns>
		public object TakeFirst()
		{
			if (this.Count == 0)
				throw new InvalidOperationException();

			object value = this[0];
			this.RemoveAt(0);
			return value;
		}

		#endregion
	}
}
