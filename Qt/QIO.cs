﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace QuasselSharp.Qt
{
	/// <summary>
	/// Read function for user types.
	/// </summary>
	/// <param name="stream"></param>
	/// <returns></returns>
	public delegate T QReadFunc<T>(Stream stream);

	/// <summary>
	/// Write function for user types.
	/// </summary>
	/// <param name="stream"></param>
	/// <param name="value"></param>
	public delegate void QWriteFunc<T>(Stream stream, T value);

	/// <summary>
	/// Helper methods for Qt input/output and (de)serialization.
	/// </summary>
	public static class QIO
	{
		#region Types

		private class UserType
		{
			public string Name;
			public Type Type;
			public Func<Stream, object> Read;
			public Action<Stream, object> Write;
		}

		#endregion

		#region Fields

		private static readonly List<UserType> userTypes;

		#endregion

		#region Constructors

		static QIO()
		{
			userTypes = new List<UserType>();
		}

		#endregion

		#region Methods

		#region Misc

		/// <summary>
		/// Adds a user type.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="read"></param>
		/// <param name="write"></param>
		public static void AddUserType<T>(string name, QReadFunc<T> read, QWriteFunc<T> write)
		{
			userTypes.Add(new UserType
			{
				Name = name,
				Type = typeof(T),
				Read = s => read(s),
				Write = (s, v) => write(s, (T)v)
			});
		}

		/// <summary>
		/// Returns the QVariantType of the object.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static QVariantType GetQVariantType(object value)
		{
			if (value is bool) return QVariantType.Bool;
			if (value is int) return QVariantType.Int;
			if (value is uint) return QVariantType.UInt;
			if (value is long) return QVariantType.LongLong;
			if (value is ulong) return QVariantType.ULongLong;
			if (value is double) return QVariantType.Double;
			if (value is char) return QVariantType.QChar;
			if (value is QVariantMap) return QVariantType.QVariantMap;
			if (value is QVariantList) return QVariantType.QVariantList;
			if (value is string) return QVariantType.QString;
			if (value is string[]) return QVariantType.QStringList;
			if (value is byte[]) return QVariantType.QByteArray;
			if (value is DateTime) return QVariantType.QDateTime; 
			return QVariantType.UserType;
		}

		/// <summary>
		/// Gets the name of a UserType QVariant.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static string GetUserTypeName(Type type)
		{
			foreach (UserType userType in userTypes)
			{
				if (userType.Type == type)
					return userType.Name;
			}

			return null;
		}

		/// <summary>
		/// Gets the type of a UserType QVariant.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public static Type GetUserType(string name)
		{
			foreach (UserType userType in userTypes)
			{
				if (userType.Name == name)
					return userType.Type;
			}

			return null;
		}

		#endregion

		#region Read
		
		/// <summary>
		/// Reads the specified number of bytes and blocks until all bytes have been read.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		/// <param name="count"></param>
		public static void ReadBlocking(this Stream stream, byte[] buffer, int offset, int count)
		{
			while (count > 0)
			{
				int numBytes = stream.Read(buffer, offset, count);
				if (numBytes == 0)
					throw new EndOfStreamException();

				offset += numBytes;
				count -= numBytes;
			}
		}

		/// <summary>
		/// Reads a 16-bit integer from the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static short ReadInt16(this Stream stream)
		{
			byte[] temp = new byte[2];
			stream.ReadBlocking(temp, 0, 2);

			return unchecked((short)(
				(temp[0] << 8) |
				(temp[1] << 0)));
		}

		/// <summary>
		/// Reads an unsigned 16-bit integer from the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static ushort ReadUInt16(this Stream stream)
		{
			byte[] temp = new byte[2];
			stream.ReadBlocking(temp, 0, 2);

			return unchecked((ushort)(
				(temp[0] << 8) |
				(temp[1] << 0)));
		}
		
		/// <summary>
		/// Reads a 32-bit integer from the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static int ReadInt32(this Stream stream)
		{
			byte[] temp = new byte[4];
			stream.ReadBlocking(temp, 0, 4);

			return
				((int)temp[0] << 24) |
				((int)temp[1] << 16) |
				((int)temp[2] << 8) |
				((int)temp[3] << 0);
		}

		/// <summary>
		/// Reads an unsigned 32-bit integer from the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static uint ReadUInt32(this Stream stream)
		{
			byte[] temp = new byte[4];
			stream.ReadBlocking(temp, 0, 4);

			return
				((uint)temp[0] << 24) |
				((uint)temp[1] << 16) |
				((uint)temp[2] << 8) |
				((uint)temp[3] << 0);
		}

		/// <summary>
		/// Reads a QChar from the the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static char ReadQChar(this Stream stream)
		{
			return (char)stream.ReadUInt16();
		}

		/// <summary>
		/// Reads a QByteArray from the stream.
		/// </summary>
		/// <returns></returns>
		public static byte[] ReadQByteArray(this Stream stream)
		{
			int length = stream.ReadInt32();
			if (length < 0) return null;

			byte[] bytes = new byte[length];
			stream.Read(bytes, 0, length);
			
			return bytes;
		}

		/// <summary>
		/// Reads a QVariantMap from the stream.
		/// </summary>
		/// <returns></returns>
		public static QVariantMap ReadQVariantMap(this Stream stream)
		{
			int count = stream.ReadInt32();

			QVariantMap map = new QVariantMap();

			for (int i = 0; i < count; i++)
			{
				string key = stream.ReadStringUTF16BE();
				object value = stream.ReadQVariant();

				map.Add(key, value);
			}

			return map;
		}

		/// <summary>
		/// Reads a QVariantList from the stream.
		/// </summary>
		/// <returns></returns>
		public static QVariantList ReadQVariantList(this Stream stream)
		{
			int count = stream.ReadInt32();

			QVariantList list = new QVariantList();

			for (int i = 0; i < count; i++)
				list.Add(stream.ReadQVariant());

			return list;
		}

		/// <summary>
		/// Reads an UTF-8 string from a stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static string ReadStringUTF8(this Stream stream)
		{
			int length = stream.ReadInt32();
			if (length < 0) return null;
			if (length == 0) return "";
			
			byte[] buffer = new byte[length];
			stream.ReadBlocking(buffer, 0, length);

			string str = Encoding.UTF8.GetString(buffer, 0, length);

			if (str.EndsWith("\0"))
				str = str.Substring(0, str.Length - 1);

			return str;
		}

		/// <summary>
		/// Reads a big endian UTF-16 string frm a stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static string ReadStringUTF16BE(this Stream stream)
		{
			int length = stream.ReadInt32();
			if (length < 0) return null;
			if (length == 0) return "";

			byte[] buffer = new byte[length];
			stream.ReadBlocking(buffer, 0, length);

			return Encoding.BigEndianUnicode.GetString(buffer);
		}

		/// <summary>
		/// Reads a QStringList from the stream and returns it as a .NET string array.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static string[] ReadQStringList(this Stream stream)
		{
			int length = stream.ReadInt32();
			string[] buffer = new string[length];

			for (int i = 0; i < length; i++)
				buffer[i] = stream.ReadStringUTF16BE();

			return buffer;
		}

		/// <summary>
		/// Reads a QDateTime from the stream and returns it as a .NET DateTime object.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static DateTime ReadQDateTime(this Stream stream)
		{
			uint julianDays = stream.ReadUInt32();
			int ms = stream.ReadInt32();

			DateTime date = julianDays == 0 ? new DateTime(0) : DateTime.FromOADate(julianDays - 2415019);
			TimeSpan time = ms < 0 ? TimeSpan.Zero : new TimeSpan(0, 0, 0, 0, ms);

			int spec = stream.ReadByte(); // timezone?
			if (spec < 0)
				throw new EndOfStreamException();

			return new DateTime(date.Year, date.Month, date.Day, time.Hours, time.Minutes, time.Seconds, time.Milliseconds);
		}

		/// <summary>
		/// Reads a QVariant from the stream and returns the 
		/// </summary>
		/// <returns></returns>
		public static object ReadQVariant(this Stream stream)
		{
			QVariantType type = (QVariantType)stream.ReadInt32();

			if (stream.ReadByte() != 0)
				return null;

			switch (type)
			{
				case QVariantType.Bool:
					return stream.ReadByte() != 0;

				case QVariantType.Int:
					return stream.ReadInt32();

				case QVariantType.UInt:
					return stream.ReadUInt32();

				case QVariantType.QChar:
					return stream.ReadQChar();

				case QVariantType.QVariantMap:
					return stream.ReadQVariantMap();

				case QVariantType.QVariantList:
					return stream.ReadQVariantList();

				case QVariantType.QString:
					return stream.ReadStringUTF16BE();

				case QVariantType.QStringList:
					return stream.ReadQStringList();

				case QVariantType.QByteArray:
					return stream.ReadQByteArray();

				case QVariantType.QDateTime:
					return stream.ReadQDateTime();

				case QVariantType.UserType:
					string typeName = stream.ReadStringUTF8();

					foreach (UserType userType in userTypes)
					{
						if (userType.Name == typeName)
							return userType.Read(stream);
					}

					throw new NotSupportedException(string.Format("User type \"{0}\" is not supported.", typeName));

				case QVariantType.Short:
					return stream.ReadInt16();

				case QVariantType.UShort:
					return stream.ReadUInt16();
			}

			throw new NotSupportedException(string.Format("QVariantType \"{0}\" is not supported.", type));
		}

		#endregion

		#region Write

		/// <summary>
		/// Writes a 16-bit integer to the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteInt16(this Stream stream, short value)
		{
			byte[] temp = new byte[2];
			temp[0] = (byte)(value >> 8);
			temp[1] = (byte)(value >> 0);
			stream.Write(temp, 0, 2);
		}

		/// <summary>
		/// Writes an unsigned 16-bit integer to the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteUInt16(this Stream stream, ushort value)
		{
			byte[] temp = new byte[2];
			temp[0] = (byte)(value >> 8);
			temp[1] = (byte)(value >> 0);
			stream.Write(temp, 0, 2);
		}

		/// <summary>
		/// Writes a 32-bit integer to the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteInt32(this Stream stream, int value)
		{
			byte[] temp = new byte[4];
			temp[0] = (byte)(value >> 24);
			temp[1] = (byte)(value >> 16);
			temp[2] = (byte)(value >> 8);
			temp[3] = (byte)(value >> 0);
			stream.Write(temp, 0, 4);
		}

		/// <summary>
		/// Writes an unsigned 32-bit integer to the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteUInt32(this Stream stream, uint value)
		{
			byte[] temp = new byte[4];
			temp[0] = (byte)(value >> 24);
			temp[1] = (byte)(value >> 16);
			temp[2] = (byte)(value >> 8);
			temp[3] = (byte)(value >> 0);
			stream.Write(temp, 0, 4);
		}

		/// <summary>
		/// Writes a QChar to the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteQChar(this Stream stream, char value)
		{
			stream.WriteUInt16((ushort)value);
		}

		/// <summary>
		/// Writes a QVariantMap to a stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="map"></param>
		public static void WriteQVariantMap(this Stream stream, QVariantMap map)
		{
			stream.WriteInt32(map.Count);

			foreach(KeyValuePair<string, object> entry in map)
			{
				stream.WriteStringUTF16BE(entry.Key);
				stream.WriteQVariant(entry.Value);
			}
		}

		/// <summary>
		/// Writes a QVariantList to a stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="list"></param>
		public static void WriteQVariantList(this Stream stream, QVariantList list)
		{
			stream.WriteInt32(list.Count);

			for (int i = 0; i < list.Count; i++)
				stream.WriteQVariant(list[i]);
		}

		/// <summary>
		/// Writes an UTF-8 string to the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteStringUTF8(this Stream stream, string value)
		{
			byte[] buffer = Encoding.UTF8.GetBytes(value);
			stream.WriteInt32(buffer.Length + 1);
			stream.Write(buffer, 0, buffer.Length);
			stream.WriteByte(0);
		}

		/// <summary>
		/// Writes a big endian UTF-16 string to the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteStringUTF16BE(this Stream stream, string value)
		{
			byte[] buffer = Encoding.BigEndianUnicode.GetBytes(value);

			stream.WriteInt32(buffer.Length);
			stream.Write(buffer, 0, buffer.Length);
		}

		/// <summary>
		/// Writes a QByteArray to the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteQByteArray(this Stream stream, byte[] value)
		{
			if (value == null)
			{
				stream.WriteInt32(-1);
				return;
			}

			stream.WriteInt32(value.Length);
			stream.Write(value, 0, value.Length);
		}

		/// <summary>
		/// Writes a QStringList to the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteQStringList(this Stream stream, string[] value)
		{
			stream.WriteInt32(value.Length);

			for (int i = 0; i < value.Length; i++)
			{
				stream.WriteStringUTF16BE(value[i]);
			}
		}

		/// <summary>
		/// Writes a QDateTime to the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteQDateTime(this Stream stream, DateTime value)
		{
			stream.WriteUInt32((uint)value.ToOADate() + 2415019u);
			stream.WriteInt32((int)value.TimeOfDay.TotalMilliseconds);
			stream.WriteByte(0);
		}

		/// <summary>
		/// Writes an object value encapsulated in a QVariant object.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteQVariant(this Stream stream, object value)
		{
			QVariantType type = QIO.GetQVariantType(value);

			stream.WriteInt32((int)type);
			stream.WriteByte(value == null ? (byte)1 : (byte)0);

			if (type == QVariantType.UserType)
			{
				string name = QIO.GetUserTypeName(value.GetType());
				stream.WriteStringUTF8(name);
			}

			stream.WriteValue(value);
		}

		/// <summary>
		/// Writes a raw value using Qt's serialization format to a stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteValue(this Stream stream, object value)
		{
			if (value is bool)
			{
				stream.WriteByte((bool)value ? (byte)1 : (byte)0);
			}
			if (value is int)
			{
				stream.WriteInt32((int)value);
			}
			else if (value is uint)
			{
				stream.WriteUInt32((uint)value);
			}
			else if (value is char)
			{
				stream.WriteQChar((char)value);
			}
			else if (value is string)
			{
				stream.WriteStringUTF16BE((string)value);
			}
			else if (value is QVariantList)
			{
				stream.WriteQVariantList((QVariantList)value);
			}
			else if (value is QVariantMap)
			{
				stream.WriteQVariantMap((QVariantMap)value);
			}
			else if (value is byte[])
			{
				stream.WriteQByteArray((byte[])value);
			}
			else if (value is DateTime)
			{
				stream.WriteQDateTime((DateTime)value);
			}
			else if (value is short)
			{
				stream.WriteInt16((short)value);
			}
			else if (value is ushort)
			{
				stream.WriteUInt16((ushort)value);
			}
			else
			{
				Type type = value.GetType();
				foreach (UserType userType in userTypes)
				{
					if (userType.Type == type)
					{
						userType.Write(stream, value);
						return;
					}
				}

				throw new NotSupportedException(string.Format("User type \"{0}\" is not supported.", type));
			}
		}

		#endregion

		#endregion
	}
}
