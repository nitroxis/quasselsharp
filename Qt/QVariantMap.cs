﻿using System;
using System.Collections.Generic;

namespace QuasselSharp.Qt
{
	/// <summary>
	/// Represents a wrapper for QVariantMap.
	/// </summary>
	public class QVariantMap : Dictionary<string, object>
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new QVariantMap.
		/// </summary>
		public QVariantMap()
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Gets a value.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key"></param>
		/// <returns></returns>
		public T Get<T>(string key)
		{
			object value;

			if (!this.TryGetValue(key, out value))
				return default(T);

			try
			{
				value = Convert.ChangeType(value, typeof(T));
			}
			catch (InvalidCastException)
			{
				return default(T);
			}

			return (T)value;
		}

		#endregion
	}
}
