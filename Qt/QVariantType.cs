﻿namespace QuasselSharp.Qt
{
	/// <summary>
	/// QVariant data types.
	/// </summary>
	public enum QVariantType
	{
		Void = 0,
		Bool = 1,
		Int = 2,
		UInt = 3,
		LongLong = 4,
		ULongLong = 5,
		Double = 6,
		QChar = 7,
		QVariantMap = 8,
		QVariantList = 9,
		QString = 10,
		QStringList = 11,
		QByteArray = 12,
		QDateTime = 16,
		UserType = 127,
		Short = 130,
		UShort = 133,
	}
}
