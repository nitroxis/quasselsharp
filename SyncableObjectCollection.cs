﻿using System.Collections.Generic;

namespace QuasselSharp
{
	/// <summary>
	/// Represents a collection of syncable objects.
	/// </summary>
	internal sealed class SyncableObjectCollection : List<SyncableObject>, ISyncableObjectContainer
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new SyncableObjectCollection.
		/// </summary>
		public SyncableObjectCollection()
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Gets a syncable object by it's class and object name.
		/// </summary>
		/// <param name="className"></param>
		/// <param name="objectName"></param>
		/// <returns></returns>
		public SyncableObject GetObject(string className, string objectName)
		{
			if (objectName != null)
			{
				int separator = objectName.IndexOf('/');
				if (separator >= 0)
				{
					string parentName = objectName.Substring(0, separator);
					string childName = objectName.Substring(separator + 1);

					foreach (SyncableObject obj in this)
					{
						ISyncableObjectContainer container = obj.Value as ISyncableObjectContainer;

						if (container != null && obj.ObjectName == parentName)
							return container.GetObject(className, childName);
					}
				}
			}

			foreach (SyncableObject obj in this)
			{
				if (obj.ClassName == className && obj.ObjectName == objectName)
					return obj;
			}

			return null;
		}


		#endregion
	}
}
