﻿namespace QuasselSharp
{
	/// <summary>
	/// Defines request types for signal proxy funcs.
	/// </summary>
	public enum RequestType
	{
		Sync = 1,
		RpcCall,
		InitRequest,
		InitData,
		HeartBeat,
		HeartBeatReply
	}
}
