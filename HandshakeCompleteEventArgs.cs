﻿using System;
using QuasselSharp.Protocol;
using QuasselSharp.Qt;

namespace QuasselSharp
{
	/// <summary>
	/// EventArgs for the HandshakeComplete event.
	/// </summary>
	public sealed class HandshakeCompleteEventArgs : EventArgs
	{
		#region Fields

		private readonly SessionState sessionState;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the received session state.
		/// </summary>
		public SessionState SessionState
		{
			get { return this.sessionState; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new HandshakeCompleteEventArgs.
		/// </summary>
		public HandshakeCompleteEventArgs(SessionState sessionState)
		{
			this.sessionState = sessionState;
		}

		#endregion

		#region Methods

		#endregion
	}
}
