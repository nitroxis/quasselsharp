﻿using System;

namespace QuasselSharp
{
	/// <summary>
	/// Message event args.
	/// </summary>
	public sealed class MessageEventArgs : EventArgs
	{
		#region Fields

		private readonly Message message;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the message.
		/// </summary>
		public Message Message
		{
			get { return this.message; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new MessageEventArgs.
		/// </summary>
		public MessageEventArgs(Message message)
		{
			this.message = message;
		}

		#endregion

		#region Methods

		#endregion
	}
}
