﻿using System;
using System.Collections.Generic;

namespace QuasselSharp
{
	/// <summary>
	/// Represents the buffer synchronizer.
	/// </summary>
	public sealed class BufferSyncer : IEnumerable<BufferInfo>
	{
		#region Events

		public event EventHandler<BufferInfoEventArgs> BufferMarkedAsRead;

		#endregion

		#region Fields

		private readonly HashSet<BufferInfo> buffers;
		private readonly Dictionary<BufferId, MessageId> lastSeenMessages;
		private readonly Dictionary<BufferId, MessageId> markerLines;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BufferSyncer.
		/// </summary>
		public BufferSyncer()
		{
			this.buffers = new HashSet<BufferInfo>();
			this.lastSeenMessages = new Dictionary<BufferId, MessageId>();
			this.markerLines = new Dictionary<BufferId, MessageId>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Adds a buffer to the buffer syncer.
		/// </summary>
		/// <param name="buffer"></param>
		public void AddBuffer(BufferInfo buffer)
		{
			this.buffers.Add(buffer);
		}

		/// <summary>
		/// Removes a buffer from the buffer syncer.
		/// </summary>
		/// <param name="buffer"></param>
		public void RemoveBuffer(BufferInfo buffer)
		{
			this.buffers.Remove(buffer);
		}

		/// <summary>
		/// Returns the buffer with the specified ID, or null if no buffer was found.
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public BufferInfo GetBuffer(BufferId id)
		{
			foreach (BufferInfo buffer in this.buffers)
			{
				if (buffer.Id == id)
					return buffer;
			}

			return null;
		}

		/// <summary>
		/// Gets the status buffer for the specified network.
		/// </summary>
		/// <param name="network"></param>
		/// <returns></returns>
		public BufferInfo GetStatusBuffer(Network network)
		{
			foreach (BufferInfo buffer in this.buffers)
			{
				if (buffer.NetworkId == network.Id && buffer.Type == BufferType.StatusBuffer)
					return buffer;
			}

			return null;
		}

		public IEnumerator<BufferInfo> GetEnumerator()
		{
			return this.buffers.GetEnumerator();
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return this.buffers.GetEnumerator();
		}

		[Sync("removeBuffer")]
		private void removeBuffer(BufferId id)
		{
			this.buffers.RemoveWhere(b => b.Id == id);
		}

		[Sync("renameBuffer")]
		private void renameBuffer(BufferId id, string name)
		{
			foreach (BufferInfo buffer in this.buffers)
			{
				if (buffer.Id == id)
				{
					buffer.Name = name;
					break;
				}
			}
		}

		[Sync("markBufferAsRead")]
		private void markBufferAsRead(BufferId bufferId)
		{
			BufferInfo buffer = this.GetBuffer(bufferId);
			if (buffer == null)
				return;

			if (this.BufferMarkedAsRead != null)
				this.BufferMarkedAsRead(this, new BufferInfoEventArgs(buffer));
		}

		[Sync("setLastSeenMsg")]
		private void setLastSeenMessage(BufferId bufferId, MessageId message)
		{
			this.lastSeenMessages[bufferId] = message;
		}

		[Sync("setMarkerLine")]
		private void setMarkerLine(BufferId bufferId, MessageId message)
		{
			this.markerLines[bufferId] = message;
		}

		#endregion
	}
}
