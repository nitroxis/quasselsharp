﻿namespace QuasselSharp
{
	/// <summary>
	/// An interface for a container of syncable objects.
	/// </summary>
	internal interface ISyncableObjectContainer
	{
		#region Properties

		SyncableObject GetObject(string className, string objectName);

		#endregion
	}
}
