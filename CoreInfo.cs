﻿using QuasselSharp.Qt;

namespace QuasselSharp
{
	/// <summary>
	/// Quassel CoreInfo object.
	/// </summary>
	public sealed class CoreInfo
	{
		#region Fields

		private QVariantMap coreData;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the data of the core.
		/// </summary>
		public QVariantMap CoreData
		{
			get { return this.coreData; }
			set { this.coreData = value; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new CoreInfo.
		/// </summary>
		public CoreInfo()
		{

		}

		#endregion

		#region Methods

		#endregion
	}
}
