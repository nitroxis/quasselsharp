﻿using System;

namespace QuasselSharp
{
	/// <summary>
	/// Represents a Quassel message.
	/// </summary>
	public sealed class Message
	{
		#region Fields

		private readonly DateTime timestamp;
		private readonly MessageId messageId;
		private readonly BufferInfo bufferInfo;
		private readonly string contents;
		private readonly string sender;
		private readonly MessageType type;
		private readonly MessageFlags flags;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the timestamp of the message.
		/// </summary>
		public DateTime Timestamp
		{
			get { return this.timestamp; }
		}

		/// <summary>
		/// Gets the ID of the message.
		/// </summary>
		public MessageId MessageId
		{
			get { return this.messageId; }
		}

		/// <summary>
		/// Gets the buffer info.
		/// </summary>
		public BufferInfo BufferInfo
		{
			get { return this.bufferInfo; }
		}

		/// <summary>
		/// Gets the contents of the message.
		/// </summary>
		public string Contents
		{
			get { return this.contents; }
		}

		/// <summary>
		/// Gets the sender of the message.
		/// </summary>
		public string Sender
		{
			get { return this.sender; }
		}

		/// <summary>
		/// Gets the message's type.
		/// </summary>
		public MessageType Type
		{
			get { return this.type; }
		}

		/// <summary>
		/// Gets the flags of the message.
		/// </summary>
		public MessageFlags Flags
		{
			get { return this.flags; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Message.
		/// </summary>
		public Message(DateTime timestamp, MessageId messageId, BufferInfo bufferInfo, string contents, string sender, MessageType type, MessageFlags flags)
		{
			this.timestamp = timestamp;
			this.messageId = messageId;
			this.bufferInfo = bufferInfo;
			this.contents = contents;
			this.sender = sender;
			this.type = type;
			this.flags = flags;
		}

		#endregion

		#region Methods

		#endregion
	}
}
