﻿namespace QuasselSharp
{
	/// <summary>
	/// Defines the buffer types.
	/// </summary>
	public enum BufferType : ushort
	{
		InvalidBuffer = 0,
		StatusBuffer = 1,
		ChannelBuffer = 2,
		QueryBuffer = 4,
		GroupBuffer = 8
	}
}
