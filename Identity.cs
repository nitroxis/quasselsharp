﻿namespace QuasselSharp
{
	/// <summary>
	/// Represents a Quassel identity.
	/// </summary>
	public sealed class Identity
	{
		#region Fields

		private IdentityId id;
		private string identityName;
		private string realName;
		private string[] nicks;
		private string awayNick;
		private bool awayNickEnabled;
		private string awayReason;
		private bool awayReasonEnabled;
		private bool autoAwayEnabled;
		private int autoAwayTime;
		private string autoAwayReason;
		private bool autoAwayReasonEnabled;
		private bool detachAwayEnabled;
		private string detachAwayReason;
		private bool detachAwayReasonEnabled;
		private string ident;
		private string kickReason;
		private string partReason;
		private string quitReason;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the identity ID.
		/// </summary>
		public IdentityId Id
		{
			get { return this.id; }
			[Sync("setId")]
			set { this.id = value; }
		}

		/// <summary>
		/// Gets or sets the display name of the identity.
		/// </summary>
		public string IdentityName
		{
			get { return this.identityName; }
			[Sync("setIdentityName")]
			set { this.identityName = value; }
		}

		public string RealName
		{
			get { return this.realName; }
			[Sync("setRealName")]
			set { this.realName = value; }
		}

		public string[] Nicks
		{
			get { return this.nicks; }
			[Sync("setNicks")]
			set { this.nicks = value; }
		}

		public string AwayNick
		{
			get { return this.awayNick; }
			[Sync("setAwayNick")]
			set { this.awayNick = value; }
		}

		public bool AwayNickEnabled
		{
			get { return this.awayNickEnabled; }
			[Sync("setAwayNickEnabled")]
			set { this.awayNickEnabled = value; }
		}

		public string AwayReason
		{
			get { return this.awayReason; }
			[Sync("setAwayReason")]
			set { this.awayReason = value; }
		}

		public bool AwayReasonEnabled
		{
			get { return this.awayReasonEnabled; }
			[Sync("setAwayReasonEnabled")]
			set { this.awayReasonEnabled = value; }
		}

		public bool AutoAwayEnabled
		{
			get { return this.autoAwayEnabled; }
			[Sync("setAutoAwayEnabled")]
			set { this.autoAwayEnabled = value; }
		}

		public int AutoAwayTime
		{
			get { return this.autoAwayTime; }
			[Sync("setAutoAwayTime")]
			set { this.autoAwayTime = value; }
		}

		public string AutoAwayReason
		{
			get { return this.autoAwayReason; }
			[Sync("setAutoAwayReason")]
			set { this.autoAwayReason = value; }
		}

		public bool AutoAwayReasonEnabled
		{
			get { return this.autoAwayReasonEnabled; }
			[Sync("setAutoAwayReasonEnabled")]
			set { this.autoAwayReasonEnabled = value; }
		}

		public bool DetachAwayEnabled
		{
			get { return this.detachAwayEnabled; }
			[Sync("setDetachAwayEnabled")]
			set { this.detachAwayEnabled = value; }
		}

		public string DetachAwayReason
		{
			get { return this.detachAwayReason; }
			[Sync("setDetachAwayReason")]
			set { this.detachAwayReason = value; }
		}

		public bool DetachAwayReasonEnabled
		{
			get { return this.detachAwayReasonEnabled; }
			[Sync("setDetachAwayReasonEnabled")]
			set { this.detachAwayReasonEnabled = value; }
		}

		public string Ident
		{
			get { return this.ident; }
			[Sync("setIdent")]
			set { this.ident = value; }
		}

		public string KickReason
		{
			get { return this.kickReason; }
			[Sync("setKickReason")]
			set { this.kickReason = value; }
		}

		public string PartReason
		{
			get { return this.partReason; }
			[Sync("setPartReason")]
			set { this.partReason = value; }
		}

		public string QuitReason
		{
			get { return this.quitReason; }
			[Sync("setQuitReason")]
			set { this.quitReason = value; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Identity.
		/// </summary>
		public Identity()
		{

		}

		#endregion

		#region Methods

		[Sync("update")]
		private void update()
		{
			
		}

		#endregion
	}
}
