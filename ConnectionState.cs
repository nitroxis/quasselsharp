﻿namespace QuasselSharp
{
	/// <summary>
	/// Defines the network connection states.
	/// </summary>
	public enum ConnectionState
	{
		Disconnected,
		Connecting,
		Initializing,
		Initialized,
		Reconnecting,
		Disconnecting
	}
}
