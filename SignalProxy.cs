﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Reflection;
using System.Text;
using QuasselSharp.Protocol;
using QuasselSharp.Qt;

namespace QuasselSharp
{
	/// <summary>
	/// Represents the signal proxy.
	/// </summary>
	public sealed class SignalProxy
	{
		#region Events

		public event EventHandler<MessageEventArgs> Message;

		#endregion

		#region Fields

		private readonly ProxyMode mode;
		private readonly List<Peer> peers;
		private readonly SyncableObjectCollection objects;
		private readonly object processLock;
		private readonly BufferSyncer bufferSyncer;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the signal proxy's mode.
		/// </summary>
		public ProxyMode Mode
		{
			get { return this.mode; }
		}

		/// <summary>
		/// Gets a list of syncable objects.
		/// </summary>
		internal SyncableObjectCollection Objects
		{
			get { return this.objects; }
		}

		public object ProcessLock
		{
			get { return this.processLock; }
		}

		/// <summary>
		/// Gets all networks.
		/// </summary>
		public IEnumerable<Network> Networks
		{
			get
			{
				lock (this.objects)
				{
					foreach (SyncableObject obj in this.objects)
					{
						if (obj.Value is Network)
							yield return (Network)obj.Value;
					}
				}
			}
		}

		/// <summary>
		/// Gets all identities.
		/// </summary>
		public IEnumerable<Identity> Identities
		{
			get
			{
				lock (this.objects)
				{
					foreach (SyncableObject obj in this.objects)
					{
						if (obj.Value is Identity)
							yield return (Identity)obj.Value;
					}
				}
			}
		}

		/// <summary>
		/// Gets the peers.
		/// </summary>
		public IEnumerable<Peer> Peers
		{
			get { return this.peers; }
		}

		/// <summary>
		/// Gets the CoreInfo object.
		/// </summary>
		public CoreInfo CoreInfo
		{
			get
			{
				lock (this.objects)
				{
					foreach (SyncableObject obj in this.objects)
					{
						if (obj.Value is CoreInfo)
							return (CoreInfo)obj.Value;
					}
				}

				return null;
			}
		}

		/// <summary>
		/// Gets the buffer syncer.
		/// </summary>
		public BufferSyncer BufferSyncer
		{
			get { return this.bufferSyncer; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new SignalProx.
		/// </summary>
		public SignalProxy(ProxyMode mode)
		{
			this.mode = mode;
			this.peers = new List<Peer>();
			this.objects = new SyncableObjectCollection();
			this.processLock = new object();
			this.bufferSyncer = new BufferSyncer();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Adds a peer.
		/// </summary>
		/// <param name="peer"></param>
		public void AddPeer(Peer peer)
		{
			if (peer == null)
				throw new ArgumentNullException("peer");

			if (this.peers.Contains(peer))
				return;

			if (peer.SignalProxy != null)
				throw new Exception("Peer already has a signal proxy.");

			this.peers.Add(peer);
			peer.SignalProxy = this;
		}

		/// <summary>
		/// Removes a peer.
		/// </summary>
		/// <param name="peer"></param>
		public void RemovePeer(Peer peer)
		{
			if (peer == null)
				throw new ArgumentNullException("peer");

			if (!this.peers.Remove(peer))
				throw new Exception("Peer has not been added.");

			peer.SignalProxy = null;
		}

		/// <summary>
		/// Handles a signal proxy message.
		/// </summary>
		/// <param name="message"></param>
		public void Handle(SignalProxyMessage message)
		{
			if (message is InitData)
				this.handleInitData((InitData)message);
			else if (message is SyncMessage)
				this.handleSyncMessage((SyncMessage)message);
			else if (message is RpcCall)
				this.handleRpcCall((RpcCall)message);
			else
				Console.Error.WriteLine("Received unsupported SignalProxy message \"{0}\".", message);
		}

		/// <summary>
		/// Adds an identity to the signal proxy.
		/// </summary>
		/// <param name="identity"></param>
		public void AddIdentity(Identity identity)
		{
			if (identity == null)
				throw new ArgumentNullException("identity");

			SyncableObject obj = new SyncableObject("Identity", ((int)identity.Id).ToString(CultureInfo.InvariantCulture), identity);
			lock (this.objects)
				this.objects.Add(obj);
		}

		/// <summary>
		/// Initializes the signal proxy (resets the state).
		/// </summary>
		public void Initialize()
		{
			lock (this.processLock)
			{
				this.objects.Clear();
				this.objects.Add(new SyncableObject("BufferSyncer", null, this.bufferSyncer));
			}
		}

		/// <summary>
		/// Initializes the signal proxy using a session state.
		/// </summary>
		/// <param name="state">The session state.</param>
		public void Initialize(SessionState state)
		{
			this.Initialize();

			// add buffers.
			foreach (BufferInfo buffer in state.BufferInfos)
				this.BufferSyncer.AddBuffer(buffer);

			// add identities.
			foreach (Identity identity in state.Identities)
				this.AddIdentity(identity);
		}

		/// <summary>
		/// Sends an RPC call.
		/// </summary>
		/// <param name="slotName"></param>
		/// <param name="parameters"></param>
		public void SendRpcCall(string slotName, params object[] parameters)
		{
			QVariantList list = new QVariantList();
			list.AddRange(parameters);

			foreach (Peer peer in this.peers)
				peer.Dispatch(new RpcCall(slotName, list));
		}

		/// <summary>
		/// Sends a message.
		/// </summary>
		/// <param name="buffer"></param>
		/// <param name="message"></param>
		public void SendMessage(BufferInfo buffer, string message)
		{
			if (buffer == null)
				throw new ArgumentNullException("buffer");

			if (message == null)
				return;

			if (message.Contains("\n"))
			{
				string[] messages = message.Split(new[] {'\n'});
				foreach (string line in messages)
					this.SendMessage(buffer, line.Trim('\r'));
			}
			else
			{
				if (!message.StartsWith("/"))
					message = "/SAY " + message;

				this.SendRpcCall("2sendInput(BufferInfo,QString)", buffer, message);
			}
		}

		/// <summary>
		/// Handles an InitData message.
		/// </summary>
		/// <param name="message"></param>
		private void handleInitData(InitData message)
		{
			object value = null;

			if (message.ClassName == "Network")
			{
				Network network = new Network((NetworkId)int.Parse(message.ObjectName));
				network.Parse(message.Data);
				value = network;
			}
			else if (message.ClassName == "CoreInfo")
			{
				CoreInfo info = new CoreInfo();
				info.CoreData = message.Data.Get<QVariantMap>("coreData");
				value = info;
			}
			else
			{
				Console.Error.WriteLine("Received InitData for unsupported class \"{0}\".", message.ClassName);
			}

			lock (this.processLock)
			{
				SyncableObject obj = this.Objects.GetObject(message.ClassName, message.ObjectName);
				if (obj != null)
				{
					lock (this.objects)
						this.objects.Remove(obj);
				}

				obj = new SyncableObject(message.ClassName, message.ObjectName, value);
				lock (this.objects)
					this.objects.Add(obj);
			}
		}

		/// <summary>
		/// Handles a synchronization message.
		/// </summary>
		/// <param name="message"></param>
		private void handleSyncMessage(SyncMessage message)
		{
			lock (this.processLock)
			{
				SyncableObject obj;
				
				lock (this.objects)
					obj = this.objects.GetObject(message.ClassName, message.ObjectName);

				if (obj == null)
				{
					Console.Error.WriteLine("Received Sync message \"{2}\" for unknown {0} \"{1}\".", message.ClassName, message.ObjectName, message.SlotName);
					return;
				}

				obj.HandleSync(message.SlotName, message.Parameters.ToArray());
			}
		}

		/// <summary>
		/// Handles a remote procedure call.
		/// </summary>
		/// <param name="message"></param>
		private void handleRpcCall(RpcCall message)
		{
			MethodInfo[] methods = this.GetType().GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

			bool found = false;

			foreach (MethodInfo method in methods)
			{
				RpcCallAttribute attribute = method.GetCustomAttribute<RpcCallAttribute>();
				if (attribute == null || attribute.Name != message.SlotName)
					continue;

				found = true;

				lock(this.processLock)
					ReflectionHelper.Invoke(this, method, message.Parameters.ToArray());
			}

			if (!found)
			{
				Console.Error.WriteLine("Received unsupported RpcCall \"{0}\".", message.SlotName);
				Console.Error.WriteLine("Parameters: {0}", message.Parameters.Count);
				for (int i = 0; i < message.Parameters.Count; i++)
					Console.Error.WriteLine(message.Parameters[i]);
			}
		}

		#region RpcCalls

		[RpcCall("2displayMsg(Message)")]
		private void displayMsg(Message message)
		{
			this.bufferSyncer.AddBuffer(message.BufferInfo);

			if (this.Message != null)
				this.Message(this, new MessageEventArgs(message));
		}
		
		[RpcCall("2displayStatusMsg(QString,QString)")]
		private void displayStatusMsg(string networkName, string message)
		{
			foreach (Network network in this.Networks)
			{
				if (network.NetworkName == networkName)
					network.OnStatusMessage(new StatusMessageEventArgs(message));
			}
		}

		[RpcCall("__objectRenamed__")]
		private void objectRenamed(byte[] className, string newName, string oldName)
		{
			string classNameStr = Encoding.UTF8.GetString(className);

			SyncableObject obj = this.Objects.GetObject(classNameStr, oldName);
			if (obj == null)
			{
				Console.Error.WriteLine("Tried to rename unknown {0} \"{1}\" to \"{2}\".", classNameStr, oldName, newName);
				return;
			}

			int separator = newName.IndexOf('/');
			if (separator >= 0)
				obj.ObjectName = newName.Substring(separator + 1);
			else
				obj.ObjectName = newName;
		}

		#endregion

		#endregion
	}
}
