using System;

namespace QuasselSharp
{
	/// <summary>
	/// Defines the message types.
	/// </summary>
	[Flags]
	public enum MessageType
	{
		Plain = 0x1,
		Notice = 0x2,
		Action = 0x4,
		Nick = 0x8,
		Mode = 0x10,
		Join = 0x20,
		Part = 0x40,
		Quit = 0x80,
		Kick = 0x100,
		Kill = 0x200,
		Server = 0x400,
		Info = 0x800,
		@Error = 0x1000,
		DayChange = 0x2000,
		Topic = 0x4000,
		NetsplitJoin = 0x8000,
		NetsplitQuit = 0x10000,
		Invite = 0x20000
	}
}
