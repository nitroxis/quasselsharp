﻿using System;

namespace QuasselSharp
{
	/// <summary>
	/// Defines message flags.
	/// </summary>
	[Flags]
	public enum MessageFlags : byte
	{
		None = 0x00,
		Self = 0x01,
		Highlight = 0x02,
		Redirected = 0x04,
		ServerMessage = 0x08,
		Backlog = 0x80
	}
}
