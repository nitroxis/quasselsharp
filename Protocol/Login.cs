﻿namespace QuasselSharp.Protocol
{
	/// <summary>
	/// Login message.
	/// </summary>
	public sealed class Login : HandshakeMessage
	{
		#region Fields

		public readonly string User;
		public readonly string Password;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Login.
		/// </summary>
		public Login(string user, string password)
		{
			this.User = user;
			this.Password = password;
		}

		#endregion
	}
}
