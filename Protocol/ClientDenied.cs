﻿namespace QuasselSharp.Protocol
{
	/// <summary>
	/// ClientDenied message.
	/// </summary>
	public sealed class ClientDenied : HandshakeMessage
	{
		#region Fields

		public readonly string ErrorString;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ClientDenied.
		/// </summary>
		public ClientDenied(string errorString)
		{
			this.ErrorString = errorString;
		}

		#endregion
	}
}
