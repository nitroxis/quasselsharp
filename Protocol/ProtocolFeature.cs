﻿using System;

namespace QuasselSharp.Protocol
{
	/// <summary>
	/// Protocol features.
	/// </summary>
	[Flags]
	public enum ProtocolFeature
	{
		Encryption = 0x01,
		Compression = 0x02
	}
}
