﻿using System.Collections.Generic;
using QuasselSharp.Qt;

namespace QuasselSharp.Protocol
{
	/// <summary>
	/// SetupData message.
	/// </summary>
	public sealed class SetupData : HandshakeMessage
	{
		#region Fields

		public readonly string AdminUser;
		public readonly string AdminPassword;
		public readonly string Backend;
		public readonly QVariantMap Data;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new SetupData.
		/// </summary>
		public SetupData(string adminUser, string adminPassword, string backend, QVariantMap data)
		{
			this.AdminUser = adminUser;
			this.AdminPassword = adminPassword;
			this.Backend = backend;
			this.Data = data;
		}

		#endregion
	}
}
