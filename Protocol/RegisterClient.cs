﻿namespace QuasselSharp.Protocol
{
	/// <summary>
	/// RegisterClient message.
	/// </summary>
	public sealed class RegisterClient : HandshakeMessage
	{
		#region Fields

		public readonly string ClientVersion;
		public readonly string BuildDate;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new RegisterClient.
		/// </summary>
		public RegisterClient(string clientVersion, string buildDate)
		{
			this.ClientVersion = clientVersion;
			this.BuildDate = buildDate;
		}

		#endregion
	}
}
