﻿using System.Collections.Generic;
using QuasselSharp.Qt;

namespace QuasselSharp.Protocol
{
	/// <summary>
	/// ClientRegistered message.
	/// </summary>
	public sealed class ClientRegistered : HandshakeMessage
	{
		#region Fields

		public readonly uint CoreFeatures;
		public readonly bool CoreConfigured;
		public readonly QVariantList BackendInfo;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ClientRegistered.
		/// </summary>
		public ClientRegistered(uint coreFeatures, bool coreConfigured, QVariantList backendInfo)
		{
			this.CoreFeatures = coreFeatures;
			this.CoreConfigured = coreConfigured;
			this.BackendInfo = backendInfo;
		}

		#endregion
	}
}
