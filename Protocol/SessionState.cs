﻿using QuasselSharp.Qt;

namespace QuasselSharp.Protocol
{
	/// <summary>
	/// SessionState message.
	/// </summary>
	public sealed class SessionState : HandshakeMessage
	{
		#region Fields

		public readonly QVariantList Identities;
		public readonly QVariantList BufferInfos;
		public readonly QVariantList NetworkIds;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new SessionState.
		/// </summary>
		public SessionState(QVariantList identities, QVariantList bufferInfos, QVariantList networkIds)
		{
			this.Identities = identities;
			this.BufferInfos = bufferInfos;
			this.NetworkIds = networkIds;
		}

		#endregion
	}
}
