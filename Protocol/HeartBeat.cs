﻿using System;

namespace QuasselSharp.Protocol
{
	/// <summary>
	/// HeartBeat message.
	/// </summary>
	public sealed class HeartBeat : PeerMessage
	{
		#region Fields

		public readonly DateTime Timestamp;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new HeartBeat.
		/// </summary>
		public HeartBeat(DateTime timestamp)
		{
			this.Timestamp = timestamp;
		}

		#endregion
	}
}
