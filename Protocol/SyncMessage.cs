﻿using QuasselSharp.Qt;

namespace QuasselSharp.Protocol
{
	/// <summary>
	/// Sync message.
	/// </summary>
	public sealed class SyncMessage : SignalProxyMessage
	{
		#region Fields

		public readonly string ClassName;
		public readonly string ObjectName;
		public readonly string SlotName;
		public readonly QVariantList Parameters;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new SyncMessage.
		/// </summary>
		public SyncMessage(string className, string objectName, string slotName, QVariantList parameters)
		{
			this.ClassName = className;
			this.ObjectName = objectName;
			this.SlotName = slotName;
			this.Parameters = parameters;
		}

		#endregion
	}
}
