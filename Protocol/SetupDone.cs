﻿namespace QuasselSharp.Protocol
{
	/// <summary>
	/// SetupDone message.
	/// </summary>
	public sealed class SetupDone : HandshakeMessage
	{
		#region Constructors

		/// <summary>
		/// Creates a new SetupDone.
		/// </summary>
		public SetupDone()
		{

		}

		#endregion
	}
}
