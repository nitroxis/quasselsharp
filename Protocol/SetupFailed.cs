﻿namespace QuasselSharp.Protocol
{
	/// <summary>
	/// SetupFailed message.
	/// </summary>
	public sealed class SetupFailed : HandshakeMessage
	{
		#region Fields

		public readonly string ErrorString;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new SetupFailed.
		/// </summary>
		public SetupFailed(string errorString)
		{
			this.ErrorString = errorString;
		}
		
		#endregion
	}
}
