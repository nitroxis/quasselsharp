﻿namespace QuasselSharp.Protocol
{
	/// <summary>
	/// InitRequest message.
	/// </summary>
	public sealed class InitRequest : SignalProxyMessage
	{
		#region Fields

		public readonly string ClassName;
		public readonly string ObjectName;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new InitRequest.
		/// </summary>
		public InitRequest(string className, string objectName)
		{
			this.ClassName = className;
			this.ObjectName = objectName;
		}

		#endregion
	}
}
