﻿namespace QuasselSharp.Protocol
{
	/// <summary>
	/// Represents an abstract handshake message.
	/// </summary>
	public abstract class HandshakeMessage : PeerMessage
	{
		#region Constructors

		/// <summary>
		/// Creates a new HandshakeMessage.
		/// </summary>
		protected HandshakeMessage()
		{

		}

		#endregion
	}
}
