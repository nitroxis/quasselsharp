﻿namespace QuasselSharp.Protocol
{
	/// <summary>
	/// Protocol types.
	/// </summary>
	public enum ProtocolType
	{
		LegacyProtocol = 0x01,
		DataStreamProtocol = 0x02
	}
}
