﻿namespace QuasselSharp.Protocol
{
	/// <summary>
	/// Represents an abstract signal proxy message.
	/// </summary>
	public abstract class SignalProxyMessage : PeerMessage
	{
		#region Constructors

		/// <summary>
		/// Creates a new SignalProxyMessage.
		/// </summary>
		protected SignalProxyMessage()
		{

		}

		#endregion
	}
}
