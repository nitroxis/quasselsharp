﻿namespace QuasselSharp.Protocol
{
	/// <summary>
	/// Represents an abstract peer message.
	/// </summary>
	public abstract class PeerMessage
	{
		#region Constructors

		/// <summary>
		/// Creates a new PeerMessage.
		/// </summary>
		protected PeerMessage()
		{

		}

		#endregion
	}
}
