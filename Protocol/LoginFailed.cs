﻿namespace QuasselSharp.Protocol
{
	/// <summary>
	/// LoginFailed message.
	/// </summary>
	public sealed class LoginFailed : HandshakeMessage
	{
		#region Fields

		public readonly string ErrorString;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new LoginFailed.
		/// </summary>
		public LoginFailed(string errorString)
		{
			this.ErrorString = errorString;
		}

		#endregion
	}
}
