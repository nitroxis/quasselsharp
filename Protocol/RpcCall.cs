﻿using QuasselSharp.Qt;

namespace QuasselSharp.Protocol
{
	/// <summary>
	/// RpcCall message.
	/// </summary>
	public sealed class RpcCall : SignalProxyMessage
	{
		#region Fields

		public readonly string SlotName;
		public readonly QVariantList Parameters;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new RpcCall.
		/// </summary>
		public RpcCall(string slotName, QVariantList parameters)
		{
			this.SlotName = slotName;
			this.Parameters = parameters;
		}

		#endregion
	}
}
