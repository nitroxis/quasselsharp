﻿namespace QuasselSharp.Protocol
{
	/// <summary>
	/// LoginSuccess message.
	/// </summary>
	public sealed class LoginSuccess : HandshakeMessage
	{
		#region Constructors

		/// <summary>
		/// Creates a new LoginSuccess.
		/// </summary>
		public LoginSuccess()
		{

		}

		#endregion
	}
}
