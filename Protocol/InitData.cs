﻿using QuasselSharp.Qt;

namespace QuasselSharp.Protocol
{
	/// <summary>
	/// InitData message.
	/// </summary>
	public sealed class InitData : SignalProxyMessage
	{
		#region Fields

		public readonly string ClassName;
		public readonly string ObjectName;
		public readonly QVariantMap Data;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new InitDat.
		/// </summary>
		public InitData(string className, string objectName, QVariantMap data)
		{
			this.ClassName = className;
			this.ObjectName = objectName;
			this.Data = data;
		}

		#endregion
	}
}
