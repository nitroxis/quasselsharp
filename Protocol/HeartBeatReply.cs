﻿using System;

namespace QuasselSharp.Protocol
{
	/// <summary>
	/// HeartBeatReply message.
	/// </summary>
	public sealed class HeartBeatReply : PeerMessage
	{
		#region Fields

		public readonly DateTime Timestamp;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new HeartBeatReply.
		/// </summary>
		public HeartBeatReply(DateTime timestamp)
		{
			this.Timestamp = timestamp;
		}

		#endregion
	}
}
