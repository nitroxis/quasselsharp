﻿namespace QuasselSharp
{
	/// <summary>
	/// Represents a Quassel BufferInfo object.
	/// </summary>
	public sealed class BufferInfo
	{
		#region Fields

		private readonly BufferId id;
		private readonly NetworkId networkId;
		private readonly BufferType type;
		private readonly uint groupId;
		private string name;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the buffer's ID.
		/// </summary>
		public BufferId Id
		{
			get { return this.id; }
		}

		/// <summary>
		/// Gets the ID of the network that owns the buffer.
		/// </summary>
		public NetworkId NetworkId
		{
			get { return this.networkId; }
		}

		/// <summary>
		/// Gets the type of the buffer.
		/// </summary>
		public BufferType Type
		{
			get { return this.type; }
		}

		/// <summary>
		/// Gets the ID of the group.
		/// </summary>
		public uint GroupId
		{
			get { return this.groupId; }
		}

		/// <summary>
		/// Gets the name of the buffer.
		/// </summary>
		public string Name
		{
			get { return this.name; }
			set { this.name = value; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BufferInfo.
		/// </summary>
		public BufferInfo(BufferId id, NetworkId networkId, BufferType type, uint groupId, string name)
		{
			this.id = id;
			this.networkId = networkId;
			this.type = type;
			this.groupId = groupId;
			this.name = name;
		}

		#endregion

		#region Methods

		public override bool Equals(object obj)
		{
			BufferInfo other = obj as BufferInfo;
			if (other == null) return false;

			return this.id == other.id;
		}

		public override int GetHashCode()
		{
			return (int)this.id;
		}

		#endregion
	}
}
