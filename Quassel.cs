﻿using System;
using System.IO;
using QuasselSharp.Qt;

namespace QuasselSharp
{
	/// <summary>
	/// Quassel helper methods.
	/// </summary>
	public static class Quassel
	{
		#region Fields

		private static bool initialized;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		static Quassel()
		{
			Initialize();
		}

		#endregion

		#region Methods

		public static void Initialize()
		{
			if (initialized)
				return;
			initialized = true;
			
			QIO.AddUserType("NetworkId", s => (NetworkId)s.ReadInt32(), (s, v) => s.WriteInt32((int)v));
			QIO.AddUserType("IdentityId", s => (IdentityId)s.ReadInt32(), (s, v) => s.WriteInt32((int)v));
			QIO.AddUserType("BufferId", s => (BufferId)s.ReadInt32(), (s, v) => s.WriteInt32((int)v));
			QIO.AddUserType("MsgId", s => (MessageId)s.ReadInt32(), (s, v) => s.WriteInt32((int)v));

			QIO.AddUserType("Identity", readIdentity, writeIdentity);
			QIO.AddUserType("BufferInfo", readBufferInfo, writeBufferInfo);
			QIO.AddUserType("Message", readMessage, writeMessage);
			QIO.AddUserType("Network::Server", readServer, writeServer);
		}

		#region Identity

		private static Identity readIdentity(Stream stream)
		{
			QVariantMap map = stream.ReadQVariantMap();

			Identity identity = new Identity();
			identity.Id = map.Get<IdentityId>("identityId");
			identity.IdentityName = map.Get<string>("identityName");
			identity.RealName = map.Get<string>("realName");
			identity.Nicks = map.Get<string[]>("nicks");
			identity.AwayNick = map.Get<string>("awayNick");
			identity.AwayNickEnabled = map.Get<bool>("awayNickEnabled");
			identity.AwayReason = map.Get<string>("awayReason");
			identity.AwayReasonEnabled = map.Get<bool>("awayReasonEnabled");
			identity.AutoAwayEnabled = map.Get<bool>("autoAwayEnabled");
			identity.AutoAwayTime = map.Get<int>("autoAwayTime");
			identity.AutoAwayReason = map.Get<string>("autoAwayReason");
			identity.AutoAwayReasonEnabled = map.Get<bool>("autoAwayReasonEnabled");
			identity.DetachAwayEnabled = map.Get<bool>("detachAwayEnabled");
			identity.DetachAwayReason = map.Get<string>("detachAwayReason");
			identity.DetachAwayReasonEnabled = map.Get<bool>("detachAwayReasonEnabled");
			identity.Ident = map.Get<string>("ident");
			identity.KickReason = map.Get<string>("kickReason");
			identity.PartReason = map.Get<string>("partReason");
			identity.QuitReason = map.Get<string>("quitReason");
			return identity;
		}

		private static void writeIdentity(Stream stream, Identity identity)
		{
			throw new NotImplementedException();
		}

		#endregion

		#region BufferInfo

		private static BufferInfo readBufferInfo(Stream stream)
		{
			BufferId id = (BufferId)stream.ReadInt32();
			NetworkId networkId = (NetworkId)stream.ReadInt32();
			BufferType type = (BufferType)stream.ReadInt16();
			uint groupId = stream.ReadUInt32();
			string name = stream.ReadStringUTF8();
			return new BufferInfo(id, networkId, type, groupId, name);
		}

		private static void writeBufferInfo(Stream stream, BufferInfo bufferInfo)
		{
			stream.WriteInt32((int)bufferInfo.Id);
			stream.WriteInt32((int)bufferInfo.NetworkId);
			stream.WriteInt16((short)bufferInfo.Type);
			stream.WriteInt32((int)bufferInfo.GroupId);
			stream.WriteStringUTF8(bufferInfo.Name);
		}

		#endregion

		#region Message

		private static Message readMessage(Stream stream)
		{
			MessageId id = (MessageId)stream.ReadInt32();
			DateTime timestamp = new DateTime((stream.ReadInt32()) * 10);
			MessageType type = (MessageType)stream.ReadInt32();
			MessageFlags flags = (MessageFlags)stream.ReadByte();
			BufferInfo bufferInfo = readBufferInfo(stream);
			string sender = stream.ReadStringUTF8();
			string contents = stream.ReadStringUTF8();
			return new Message(timestamp, id, bufferInfo, contents, sender, type, flags);
		}

		private static void writeMessage(Stream stream, Message message)
		{
			
		}

		#endregion

		#region Server

		private static Server readServer(Stream stream)
		{
			QVariantMap map = stream.ReadQVariantMap();
			Server server = new Server();

			server.Host = map.Get<string>("Host");
			server.Port = map.Get<uint>("Port");
			server.Password = map.Get<string>("Password");
			server.UseSsl = map.Get<bool>("UseSSL");
			server.SslVersion = map.Get<int>("sslVersion");

			server.UseProxy = map.Get<bool>("UseProxy");
			server.ProxyType = map.Get<int>("ProxyType");
			server.ProxyHost = map.Get<string>("ProxyHost");
			server.ProxyPort = map.Get<uint>("ProxyPort");
			server.ProxyUser = map.Get<string>("ProxyUser");
			server.ProxyPassword = map.Get<string>("ProxyPass");

			return server;
		}

		private static void writeServer(Stream stream, Server server)
		{
			QVariantMap map = new QVariantMap();

			map["Host"] = server.Host;
			map["Port"] = server.Port;
			map["Password"] = server.Password;
			map["UseSSL"] = server.UseSsl;
			map["sslVersion"] = server.SslVersion;

			map["UseProxy"] = server.UseProxy;
			map["ProxyType"] = server.ProxyType;
			map["ProxyHost"] = server.ProxyHost;
			map["ProxyPort"] = server.ProxyPort;
			map["ProxyUser"] = server.ProxyUser;
			map["ProxyPass"] = server.ProxyPassword;

			stream.WriteQVariantMap(map);
		}

		#endregion

		#endregion
	}
	
	/// <summary>
	/// Represents the ID of a network.
	/// </summary>
	public enum NetworkId
	{
		Invalid = 0,
	}

	/// <summary>
	/// Represents the ID of an identity.
	/// </summary>
	public enum IdentityId
	{
		Invalid = 0,
	}

	/// <summary>
	/// Represents the ID of an buffer.
	/// </summary>
	public enum BufferId
	{
		Invalid = 0,
	}

	/// <summary>
	/// Represents the ID of a message.
	/// </summary>
	public enum MessageId
	{
		Invalid = 0,
	}
}
