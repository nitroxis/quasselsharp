﻿namespace QuasselSharp
{
	/// <summary>
	/// Represents a network server.
	/// </summary>
	public sealed class Server
	{
		#region Fields

		private string host;
		private uint port;
		private string password;
		private bool useSsl;
		private int sslVersion;

		private bool useProxy;
		private int proxyType;
		private string proxyHost;
		private uint proxyPort;
		private string proxyUser;
		private string proxyPassword;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the host of the server.
		/// </summary>
		public string Host
		{
			get { return this.host; }
			set { this.host = value; }
		}

		/// <summary>
		/// Gets or sets the port of the server.
		/// </summary>
		public uint Port
		{
			get { return this.port; }
			set { this.port = value; }
		}

		/// <summary>
		/// Gets or sets the server's password.
		/// </summary>
		public string Password
		{
			get { return this.password; }
			set { this.password = value; }
		}

		/// <summary>
		/// Gets or sets a value that determines whether to use a secure connection for this server.
		/// </summary>
		public bool UseSsl
		{
			get { return this.useSsl; }
			set { this.useSsl = value; }
		}

		/// <summary>
		/// Gets or sets the SSL version to use.
		/// </summary>
		public int SslVersion
		{
			get { return this.sslVersion; }
			set { this.sslVersion = value; }
		}

		/// <summary>
		/// Gets or sets a value that determines whether to use a proxy for this server.
		/// </summary>
		public bool UseProxy
		{
			get { return this.useProxy; }
			set { this.useProxy = value; }
		}
		
		/// <summary>
		/// Gets or sets the proxy type.
		/// </summary>
		public int ProxyType
		{
			get { return this.proxyType; }
			set { this.proxyType = value; }
		}
		
		/// <summary>
		/// Gets or sets the proxy's host.
		/// </summary>
		public string ProxyHost
		{
			get { return this.proxyHost; }
			set { this.proxyHost = value; }
		}

		/// <summary>
		/// Gets or sets the proxy's port.
		/// </summary>
		public uint ProxyPort
		{
			get { return this.proxyPort; }
			set { this.proxyPort = value; }
		}

		/// <summary>
		/// Gets or sets the user name for the proxy.
		/// </summary>
		public string ProxyUser
		{
			get { return this.proxyUser; }
			set { this.proxyUser = value; }
		}

		/// <summary>
		/// Gets or sets the password for the proxy.
		/// </summary>
		public string ProxyPassword
		{
			get { return this.proxyPassword; }
			set { this.proxyPassword = value; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Server.
		/// </summary>
		public Server()
		{
			this.port = 6667;
			this.useSsl = false;
			this.sslVersion = 0;
			this.useProxy = false;
			this.proxyHost = "localhost";
			this.proxyPort = 8080;
		}

		#endregion

		#region Methods

		#endregion
	}
}
