﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace QuasselSharp
{
	/// <summary>
	/// Represents a Quassel client.
	/// </summary>
	public class Client
	{
		#region Fields

		private TcpClient client;
		private RemotePeer peer;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the remote peer of the client.
		/// </summary>
		public RemotePeer Peer
		{
			get { return this.peer; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Client.
		/// </summary>
		public Client()
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Asynchronously connects to the specified host and port and returns the peer.
		/// </summary>
		/// <param name="host"></param>
		/// <param name="port"></param>
		/// <returns></returns>
		public async Task<RemotePeer> ConnectAsync(string host, int port)
		{
			this.client = new TcpClient();
			await this.client.ConnectAsync(host, port);
			
			Stream stream = this.client.GetStream();
			await stream.WriteAsync(new byte[]
			{
				0x42, 0xB3, 0x3F, 0x00, // magic number.
				0x80, 0x00, 0x00, 0x02 // data stream protocol.
			}, 0, 8);

			byte[] selectedProtocol = new byte[4];
			int bytes = await stream.ReadAsync(selectedProtocol, 0, 4);
			if (bytes < 4)
			{
				Console.Error.WriteLine("Invalid response from server.");
				return null;
			}
			
			Protocol.ProtocolType type = (Protocol.ProtocolType)selectedProtocol[3];
			if (type == Protocol.ProtocolType.DataStreamProtocol)
				return (this.peer = new DataStreamPeer(stream));

			Console.Error.WriteLine("Unsupported protocol \"{0}\".", type);
			this.Disconnect();
			return null;
		}

		/// <summary>
		/// Connects to the specified host and port and returns the peer.
		/// </summary>
		/// <param name="host"></param>
		/// <param name="port"></param>
		/// <returns></returns>
		public RemotePeer Connect(string host, int port)
		{
			this.client = new TcpClient();
			this.client.Connect(host, port);

			Stream stream = this.client.GetStream();
			stream.Write(new byte[]
			{
				0x42, 0xB3, 0x3F, 0x00, // magic number.
				0x80, 0x00, 0x00, 0x02 // data stream protocol.
			}, 0, 8);

			byte[] selectedProtocol = new byte[4];
			int bytes = stream.Read(selectedProtocol, 0, 4);
			if (bytes < 4)
			{
				Console.Error.WriteLine("Invalid response from server.");
				return null;
			}

			Protocol.ProtocolType type = (Protocol.ProtocolType)selectedProtocol[3];
			if (type == Protocol.ProtocolType.DataStreamProtocol)
				return (this.peer = new DataStreamPeer(stream));

			Console.Error.WriteLine("Unsupported protocol \"{0}\".", type);
			this.Disconnect();
			return null;
		}

		/// <summary>
		/// Closes the connection.
		/// </summary>
		public void Disconnect()
		{
			if (this.client == null)
				return;

			this.client.Close();
			this.client = null;
		}

		#endregion
	}
}
