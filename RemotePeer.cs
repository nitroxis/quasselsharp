﻿using System;
using System.IO;
using QuasselSharp.Protocol;
using QuasselSharp.Qt;

namespace QuasselSharp
{
	/// <summary>
	/// Represents a remote peer.
	/// </summary>
	public abstract class RemotePeer : Peer
	{
		#region Constants

		private const int maxMessageSize = 64 * 1024 * 1024;

		#endregion

		#region Fields

		private readonly Stream stream;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new RemotePeer.
		/// </summary>
		protected RemotePeer(Stream stream)
		{
			this.stream = stream;
		}

		#endregion

		#region Methods
	
		public abstract void Dispatch(HeartBeat message);
		public abstract void Dispatch(HeartBeatReply message);

		public override void Handle(PeerMessage message)
		{
			if (message is HeartBeat)
			{
				this.Dispatch(new HeartBeatReply(((HeartBeat)message).Timestamp));
			}
			else if (message is HeartBeatReply)
			{
				// TODO
			}
			else
			{
				base.Handle(message);	
			}
		}

		/// <summary>
		/// Writes data to the stream.
		/// </summary>
		/// <param name="data"></param>
		protected void WriteMessage(byte[] data)
		{
			this.stream.WriteInt32(data.Length);
			this.stream.Write(data, 0, data.Length);
		}

		/// <summary>
		/// Processes a message.
		/// </summary>
		/// <param name="data"></param>
		protected abstract void ProcessMessage(byte[] data);

		public bool ReadMessage()
		{
			int msgSize = this.stream.ReadInt32();

			if (msgSize > maxMessageSize)
				throw new InvalidDataException("Peer tried to send package larger than max package size!");

			if (msgSize == 0)
				throw new InvalidDataException("Peer tried to send an empty message!");

			byte[] data = new byte[msgSize];
			this.stream.ReadBlocking(data, 0, msgSize);

			this.ProcessMessage(data);

			return true;
		}

		#endregion
	}
}
