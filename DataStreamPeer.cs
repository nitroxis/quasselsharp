﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using QuasselSharp.Protocol;
using QuasselSharp.Qt;

namespace QuasselSharp
{
	/// <summary>
	/// Represents a peer of the DataStream protocol.
	/// </summary>
	public sealed class DataStreamPeer : RemotePeer
	{	
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new DataStreamPeer.
		/// </summary>
		public DataStreamPeer(Stream stream)
			: base(stream)
		{
			Quassel.Initialize();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Processes a message.
		/// </summary>
		/// <param name="data"></param>
		protected override void ProcessMessage(byte[] data)
		{
			MemoryStream stream = new MemoryStream(data, false);
			QVariantList list = stream.ReadQVariantList();

			if (!this.AuthHandler.Complete)
				this.handleHandshakeMessage(list);
			else
				this.handlePackedFunc(list);
		}

		/// <summary>
		/// Handles a handshake message.
		/// </summary>
		/// <param name="list"></param>
		private void handleHandshakeMessage(QVariantList list)
		{
			QVariantMap m = new QVariantMap();

			// convert map to list (for some reason it's always sent as list).
			for (int i = 0; i < list.Count; i += 2)
			{
				string key;
				if (list[i] is byte[])
					key = Encoding.UTF8.GetString((byte[])list[i]);
				else
					key = (string)list[i];

				m.Add(key, list[i + 1]);
			}

			// parse message.
			string messageType = (string)m["MsgType"];
			if (string.IsNullOrWhiteSpace(messageType)) throw new InvalidDataException("Invalid handshake message!");
			else if (messageType == "ClientInit") this.Handle(new RegisterClient((string)m["ClientVersion"], (string)m["ClientDate"]));
			else if (messageType == "ClientInitReject") this.Handle(new ClientDenied((string)m["Error"]));
			else if (messageType == "ClientInitAck") this.Handle(new ClientRegistered((uint)m["CoreFeatures"], (bool)m["Configured"], (QVariantList)m["StorageBackends"]));
			else if (messageType == "ClientSetupData") this.Handle(new SetupData((string)m["AdminUser"], (string)m["AdminPassword"], (string)m["Backend"], (QVariantMap)m["ConnectionProperties"]));
			else if (messageType == "CoreSetupReject") this.Handle(new SetupFailed((string)m["Error"]));
			else if (messageType == "CoreSetupAck") this.Handle(new SetupDone());
			else if (messageType == "ClientLogin") this.Handle(new Login((string)m["User"], (string)m["Password"]));
			else if (messageType == "ClientLoginReject") this.Handle(new LoginFailed((string)m["Error"]));
			else if (messageType == "ClientLoginAck") this.Handle(new LoginSuccess());
			else if (messageType == "SessionInit") this.Handle(new SessionState((QVariantList)((QVariantMap)m["SessionState"])["Identities"], (QVariantList)((QVariantMap)m["SessionState"])["BufferInfos"], (QVariantList)((QVariantMap)m["SessionState"])["NetworkIds"]));
			else throw new InvalidDataException(string.Format("Unknown protocol message of type {0}.", messageType));
		}

		private void handlePackedFunc(QVariantList packedFunc)
		{
			// read request type.
			RequestType type = (RequestType)packedFunc.TakeFirst();

			Func<byte[], string> toString = bytes =>
			{
				if (bytes == null) return null;
				if (bytes.Length == 0) return "";

				return Encoding.UTF8.GetString(bytes);
			};

			if (type == RequestType.Sync)
			{
				if (packedFunc.Count < 3)
					throw new Exception("Received invalid sync call.");

				string className = toString((byte[])packedFunc.TakeFirst());
				string objectName = toString((byte[])packedFunc.TakeFirst());
				string slotName = toString((byte[])packedFunc.TakeFirst());

				this.Handle(new SyncMessage(className, objectName, slotName, packedFunc));
			}
			else if (type == RequestType.RpcCall)
			{
				if (packedFunc.Count == 0)
					throw new Exception("Received empty RPC call.");

				string slotName = toString((byte[])packedFunc.TakeFirst());

				this.Handle(new RpcCall(slotName, packedFunc));
			}
			else if (type == RequestType.InitRequest)
			{
				if (packedFunc.Count != 2)
					throw new Exception("Received invalid InitRequest.");

				string className = toString((byte[])packedFunc.TakeFirst());
				string objectName = toString((byte[])packedFunc.TakeFirst());

				this.Handle(new InitRequest(className, objectName));
			}
			else if (type == RequestType.InitData)
			{
				if (packedFunc.Count < 2)
					throw new Exception("Received invalid InitData.");

				string className = toString((byte[])packedFunc.TakeFirst());
				string objectName = toString((byte[])packedFunc.TakeFirst());

				QVariantMap initData = new QVariantMap();
				for (int i = 0; i < packedFunc.Count / 2; i++)
					initData[Encoding.UTF8.GetString((byte[])packedFunc[2 * i])] = packedFunc[2 * i + 1];

				this.Handle(new InitData(className, objectName, initData));
			}
			else if (type == RequestType.HeartBeat)
			{
				if (packedFunc.Count != 1)
					throw new Exception("Received invalid HeartBeat.");

				this.Handle(new HeartBeat((DateTime)packedFunc[0]));
			}
			else if (type == RequestType.HeartBeatReply)
			{
				if (packedFunc.Count != 1)
					throw new Exception("Received invalid HeartBeatReply.");

				this.Handle(new HeartBeatReply((DateTime)packedFunc[0])); 
			}
			else
			{
				throw new Exception(string.Format("Received invalid or unsupported request type \"{0}\".", type));
			}
		}

		private void writeMessage(QVariantMap handshakeMsg)
		{
			QVariantList list = new QVariantList();
			foreach (KeyValuePair<string, object> item in handshakeMsg)
			{
				list.Add(item.Key);
				list.Add(item.Value);
			}

			this.writeMessage(list);
		}

		private void writeMessage(QVariantList sigProxyMsg)
		{
			MemoryStream data = new MemoryStream();
			data.WriteQVariantList(sigProxyMsg);

			byte[] buffer = data.GetBuffer();
			Array.Resize(ref buffer, (int)data.Length);
			this.WriteMessage(buffer);
		}

		public override void Dispatch(RegisterClient message)
		{
			QVariantMap m = new QVariantMap();
			m["MsgType"] = "ClientInit";
			m["ClientVersion"] = message.ClientVersion;
			m["ClientDate"] = message.BuildDate;
			this.writeMessage(m);
		}

		public override void Dispatch(ClientDenied message)
		{
			QVariantMap m = new QVariantMap();
			m["MsgType"] = "ClientInitReject";
			m["Error"] = message.ErrorString;
			this.writeMessage(m);
		}

		public override void Dispatch(ClientRegistered message)
		{
			QVariantMap m = new QVariantMap();
			m["MsgType"] = "ClientInitAck";
			m["CoreFeatures"] = message.CoreFeatures;
			m["StorageBackends"] = message.BackendInfo;
			m["LoginEnabled"] = m["Configured"] = message.CoreConfigured;
			this.writeMessage(m);
		}

		public override void Dispatch(SetupData message)
		{
			QVariantMap map = new QVariantMap();
			map["AdminUser"] = message.AdminUser;
			map["AdminPasswd"] = message.AdminPassword;
			map["Backend"] = message.Backend;
			map["ConnectionProperties"] = message.Data;

			QVariantMap m = new QVariantMap();
			m["MsgType"] = "CoreSetupData";
			m["SetupData"] = map;

			this.writeMessage(m);
		}

		public override void Dispatch(SetupFailed message)
		{
			QVariantMap m = new QVariantMap();
			m["MsgType"] = "CoreSetupReject";
			m["Error"] = message.ErrorString;
			this.writeMessage(m);
		}

		public override void Dispatch(SetupDone message)
		{
			QVariantMap m = new QVariantMap();
			m["MsgType"] = "CoreSetupAck";
			this.writeMessage(m);
		}

		public override void Dispatch(Login message)
		{
			QVariantMap m = new QVariantMap();
			m["MsgType"] = "ClientLogin";
			m["User"] = message.User;
			m["Password"] = message.Password;
			this.writeMessage(m);
		}

		public override void Dispatch(LoginFailed message)
		{
			QVariantMap m = new QVariantMap();
			m["MsgType"] = "ClientLoginReject";
			m["User"] = message.ErrorString;
			this.writeMessage(m);
		}

		public override void Dispatch(LoginSuccess message)
		{
			QVariantMap m = new QVariantMap();
			m["MsgType"] = "ClientLoginAck";
			this.writeMessage(m);
		}

		public override void Dispatch(SessionState message)
		{
			QVariantMap m = new QVariantMap();
			m["MsgType"] = "SessionInit";

			QVariantMap map = new QVariantMap();
			map["BufferInfos"] = message.BufferInfos;
			map["NetworkIds"] = message.NetworkIds;
			map["Identities"] = message.Identities;
			m["SessionState"] = map;

			this.writeMessage(m);
		}

		public override void Dispatch(SyncMessage message)
		{
			throw new NotImplementedException();
		}

		public override void Dispatch(RpcCall message)
		{
			QVariantList list = new QVariantList();
			list.Add((int)RequestType.RpcCall);
			list.Add(message.SlotName);
			list.AddRange(message.Parameters);
			this.writeMessage(list);
		}

		public override void Dispatch(InitRequest message)
		{
			QVariantList list = new QVariantList();
			list.Add((int)RequestType.InitRequest);
			list.Add(message.ClassName);
			list.Add(Encoding.UTF8.GetBytes(message.ObjectName));
			this.writeMessage(list);
		}

		public override void Dispatch(InitData message)
		{
			throw new NotImplementedException();
		}

		public override void Dispatch(HeartBeat message)
		{
			QVariantList list = new QVariantList();
			list.Add((int)RequestType.HeartBeat);
			list.Add(message.Timestamp);
			this.writeMessage(list);
		}

		public override void Dispatch(HeartBeatReply message)
		{
			QVariantList list = new QVariantList();
			list.Add((int)RequestType.HeartBeatReply);
			list.Add(message.Timestamp);
			this.writeMessage(list);
		}
		
		#endregion
	}
}
