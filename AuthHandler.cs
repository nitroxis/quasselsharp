﻿using System;
using QuasselSharp.Protocol;

namespace QuasselSharp
{
	/// <summary>
	/// Represents an abstract authentication handler.
	/// </summary>
	public abstract class AuthHandler
	{
		#region Events

		public event EventHandler<HandshakeCompleteEventArgs> HandshakeComplete;

		#endregion

		#region Fields

		private bool complete;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets a value indicating whether the authentication is complete.
		/// </summary>
		public bool Complete
		{
			get { return this.complete; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new AuthHandler.
		/// </summary>
		protected AuthHandler()
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Handles a handshake message with using this auth handler.
		/// </summary>
		/// <param name="message"></param>
		public abstract void Handle(HandshakeMessage message);

		protected void OnHandshakeComplete(HandshakeCompleteEventArgs e)
		{
			this.complete = true;

			if (this.HandshakeComplete != null)
				this.HandshakeComplete(this, e);
		}

		#endregion
	}
}
