﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace QuasselSharp
{
	public delegate void SyncHandler(object target, object[] parameters);

	/// <summary>
	/// Represnts a wrapper for syncable objects.
	/// </summary>
	internal sealed class SyncableObject
	{
		#region Fields

		private readonly string className;
		private string objectName;
		private readonly object value;

		private readonly ReadOnlyDictionary<string, SyncHandler> syncHandlers;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the object's class name.
		/// </summary>
		public string ClassName
		{
			get { return this.className; }
		}

		/// <summary>
		/// Gets the object's name.
		/// </summary>
		public string ObjectName
		{
			get { return this.objectName; }
			set { this.objectName = value; }
		}

		/// <summary>
		/// Gets the underlying object instance.
		/// </summary>
		public object Value
		{
			get { return this.value; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new SyncableObject.
		/// </summary>
		public SyncableObject(string className, string objectName, object value)
		{
			this.className = className;
			this.objectName = objectName;
			this.value = value;

			if (value != null)
				this.syncHandlers = ReflectionHelper.GetSyncHandlers(value.GetType());
		}

		#endregion

		#region Methods

		/// <summary>
		/// Handles a synchronization call for the underlying object.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="parameters"></param>
		public void HandleSync(string name, object[] parameters)
		{
			SyncHandler handler;

			if (!this.syncHandlers.TryGetValue(name, out handler))
			{
				Console.Error.WriteLine("Received unsupported Sync message \"{0}\" for {1}.", name, this.className);
				return;
			}

			handler(this.value, parameters);
		}

		public override string ToString()
		{
			return string.Format("{0} {1}", this.className, this.objectName);
		}
		
		#endregion
	}
}
