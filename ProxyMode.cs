﻿namespace QuasselSharp
{
	/// <summary>
	/// Defines the signal proxy modes.
	/// </summary>
	public enum ProxyMode
	{
		Server,
		Client
	}
}
