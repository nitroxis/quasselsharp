﻿using System;
using System.Collections.Generic;

namespace QuasselSharp
{
	/// <summary>
	/// Represents an IRC user.
	/// </summary>
	public sealed class IrcUser
	{
		#region Nested Types

		public class EqualityComparer : IEqualityComparer<IrcUser>
		{
			public bool Equals(IrcUser x, IrcUser y)
			{
				if (x.network.Id != y.network.Id)
					return false;
				return Irc.ToLowerCase(x.nick) == Irc.ToLowerCase(y.nick);
			}

			public int GetHashCode(IrcUser obj)
			{
				return obj.nick.GetHashCode() ^ (int)obj.network.Id;
			}
		}

		#endregion

		#region Fields

		private readonly Network network;
		private readonly List<IrcChannel> channels;
		
		private string user;
		private string host;
		private string nick;
		private string realName;
		private bool away;
		private string awayMessage;
		private DateTime idleTime;
		private DateTime loginTime;
		private string server;
		private string ircOperator;
		private int lastAwayMessage;
		private string whoisServiceReply;
		private string suserHost;
		private bool encrypted;
		private string userModes;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the network.
		/// </summary>
		public Network Network
		{
			get { return this.network; }
		}

		/// <summary>
		/// Gets or sets the user name.
		/// </summary>
		public string User
		{
			get { return this.user; }
			[Sync("setUser")]
			set { this.user = value; }
		}

		/// <summary>
		/// Gets or sets the host name.
		/// </summary>
		public string Host
		{
			get { return this.host; }
			[Sync("setHost")]
			set { this.host = value; }
		}

		/// <summary>
		/// Gets or sets the nick name.
		/// </summary>
		public string Nick
		{
			get { return this.nick; }
			[Sync("setNick")]
			set { this.nick = value; }
		}

		/// <summary>
		/// Gets or sets the real name.
		/// </summary>
		public string RealName
		{
			get { return this.realName; }
			[Sync("setRealName")]
			set { this.realName = value; }
		}

		/// <summary>
		/// Gets or sets the full name of the user in the nick!user@host format.
		/// </summary>
		public string FullName
		{
			get { return this.nick + "!" + this.user + "@" + this.host; }
			[Sync("updateHostmask")]
			set
			{
				int sep = value.IndexOf('@');
				if (sep >= 0)
				{
					this.User = value.Substring(0, sep);
					this.Host = value.Substring(sep + 1);

					sep = this.User.IndexOf('!');
					if (sep >= 0)
					{
						this.Nick = this.User.Substring(0, sep);
						this.User = this.User.Substring(sep + 1);
					}
				}
				else
				{
					this.User = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets a value that indicates whether the user is away.
		/// </summary>
		public bool Away
		{
			get { return this.away; }
			[Sync("setAway")]
			set { this.away = value; }
		}

		/// <summary>
		/// Gets or sets the away message.
		/// </summary>
		public string AwayMessage
		{
			get { return this.awayMessage; }
			[Sync("setAwayMessage")]
			set { this.awayMessage = value; }
		}

		/// <summary>
		/// Gets or sets the idle time.
		/// </summary>
		public DateTime IdleTime
		{
			get { return this.idleTime; }
			[Sync("setIdleTime")]
			set { this.idleTime = value; }
		}

		/// <summary>
		/// Gets or sets the login time.
		/// </summary>
		public DateTime LoginTime
		{
			get { return this.loginTime; }
			[Sync("setLoginTime")]
			set { this.loginTime = value; }
		}

		/// <summary>
		/// Gets or sets the name of the server the user is using.
		/// </summary>
		public string Server
		{
			get { return this.server; }
			[Sync("setServer")]
			set { this.server = value; }
		}

		/// <summary>
		/// Gets or sets the IRC operator.
		/// </summary>
		public string IrcOperator
		{
			get { return this.ircOperator; }
			[Sync("setIrcOperator")]
			set { this.ircOperator = value; }
		}

		public int LastAwayMessage
		{
			get { return this.lastAwayMessage; }
			[Sync("setLastAwayMessage")]
			set { this.lastAwayMessage = value; }
		}

		public string WhoisServiceReply
		{
			get { return this.whoisServiceReply; }
			[Sync("setWhoisServiceReply")]
			set { this.whoisServiceReply = value; }
		}

		public string SuserHost
		{
			get { return this.suserHost; }
			[Sync("setSuserHost")]
			set { this.suserHost = value; }
		}

		public bool Encrypted
		{
			get { return this.encrypted; }
			[Sync("setEncrypted")]
			set { this.encrypted = value; }
		}

		public string UserModes
		{
			get { return this.userModes; }
			[Sync("setUserModes")]
			set { this.userModes = value; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new IrcUser.
		/// </summary>
		public IrcUser(Network network)
		{
			this.network = network;
			this.channels = new List<IrcChannel>();
		}

		/// <summary>
		/// Creates a new IrcUser.
		/// </summary>
		public IrcUser(Network network, string fullName)
			: this(network)
		{
			this.FullName = fullName;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Joins the specified channel.
		/// </summary>
		/// <param name="channel"></param>
		public void JoinChannel(IrcChannel channel)
		{
			if (!this.channels.Contains(channel))
			{
				this.channels.Add(channel);
				channel.JoinIrcUser(this);
			}
		}

		[Sync("joinChannel")]
		private void joinChannel(string channelName)
		{
			
		}

		/// <summary>
		/// Parts the specified channel.
		/// </summary>
		/// <param name="channel"></param>
		public void PartChannel(IrcChannel channel)
		{
			if (this.channels.Remove(channel))
			{
				channel.Part(this);	
			}
		}

		[Sync("partChannel")]
		private void partChannel(string channelName)
		{

		}

		/// <summary>
		/// Removes the user from all channels and the network.
		/// </summary>
		[Sync("quit")]
		public void Quit()
		{
			// part all channels.
			while (this.channels.Count > 0)
				this.PartChannel(this.channels[0]);

			// remove user.
			this.network.RemoveIrcUser(this);
		}

		[Sync("addUserModes")]
		public void AddUserModes(string modes)
		{
			if (this.userModes == null)
				this.userModes = "";

			for (int i = 0; i < modes.Length; i++)
			{
				if (this.userModes.IndexOf(modes[i]) < 0)
					this.userModes += modes[i];
			}
		}

		[Sync("removeUserModes")]
		public void RemoveUserModes(string modes)
		{
			if (this.userModes == null)
			{
				this.userModes = "";
				return;
			}

			for (int i = 0; i < modes.Length; i++)
			{
				int index = this.userModes.IndexOf(modes[i]);
				if (index >= 0)
					this.userModes = this.userModes.Remove(index, 1);
			}
		}
		
		public override string ToString()
		{
			return this.nick;
		}

		#endregion
	}
}
