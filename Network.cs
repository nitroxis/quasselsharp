﻿using System;
using System.Collections.Generic;
using System.Text;
using QuasselSharp.Qt;

namespace QuasselSharp
{
	/// <summary>
	/// Represents a Quassel network.
	/// </summary>
	public sealed class Network : ISyncableObjectContainer
	{
		#region Events

		public event EventHandler<StatusMessageEventArgs> StatusMessage;

		#endregion

		#region Fields

		private readonly NetworkId id;
		private string networkName;
		private string currentServer;
		private string myNick;
		private int latency;
		private Encoding codecForServer;
		private Encoding codecForEncoding;
		private Encoding codecForDecoding;
		private IdentityId identityId;
		private bool isConnected;
		private ConnectionState connectionState;
		private bool useRandomServer;
		private string[] perform;
		private bool useAutoIdentify;
		private string autoIdentifyService;
		private string autoIdentifyPassword;
		private bool useSasl;
		private string saslAccount;
		private string saslPassword;
		private bool useAutoReconnect;
		private uint autoReconnectInterval;
		private ushort autoReconnectRetries;
		private bool unlimitedReconnectRetries;
		private bool rejoinChannels;

		private readonly List<Server> serverList;
		private readonly Dictionary<string, string> supports;

		private readonly List<IrcUser> users;
		private readonly List<IrcChannel> channels;
		private readonly SyncableObjectCollection objects;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets the ID of the network.
		/// </summary>
		public NetworkId Id
		{
			get { return this.id; }
		}

		/// <summary>
		/// Gets or sets the name of the network.
		/// </summary>
		public string NetworkName
		{
			get { return this.networkName; }
			[Sync("setNetworkName")]
			set { this.networkName = value; }
		}

		/// <summary>
		/// Gets or sets the current server.
		/// </summary>
		public string CurrentServer
		{
			get { return this.currentServer; }
			[Sync("setCurrentServer")]
			set { this.currentServer = value; }
		}

		/// <summary>
		/// Gets or sets the nick of the client.
		/// </summary>
		public string MyNick
		{
			get { return this.myNick; }
			[Sync("setMyNick")]
			set { this.myNick = value; }
		}

		/// <summary>
		/// Gets or sets the latency of the network.
		/// </summary>
		public int Latency
		{
			get { return this.latency; }
			[Sync("setLatency")]
			set { this.latency = value; }
		}

		/// <summary>
		/// Get or sets the encoding that is used to communicate with the IRC server.
		/// </summary>
		public Encoding CodecForServer
		{
			get { return this.codecForServer; }
			set { this.codecForServer = value; }
		}

		/// <summary>
		/// Get or sets the encoding that is used to encode messages.
		/// </summary>
		public Encoding CodecForEncoding
		{
			get { return this.codecForEncoding; }
			set { this.codecForEncoding = value; }
		}

		/// <summary>
		/// Get or sets the encoding that is used to decode messages.
		/// </summary>
		public Encoding CodecForDecoding
		{
			get { return this.codecForDecoding; }
			set { this.codecForDecoding = value; }
		}

		/// <summary>
		/// Gets or sets the identity ID of the network.
		/// </summary>
		public IdentityId IdentityId
		{
			get { return this.identityId; }
			[Sync("setIdentity")]
			set { this.identityId = value; }
		}

		/// <summary>
		/// Gets or sets a value indicating whether the network is connected.
		/// </summary>
		public bool IsConnected
		{
			get { return this.isConnected; }
			[Sync("setConnected")]
			set { this.isConnected = value; }
		}

		/// <summary>
		/// Gets or sets the connection state of the network.
		/// </summary>
		public ConnectionState ConnectionState
		{
			get { return this.connectionState; }
			[Sync("setConnectionState")]
			set { this.connectionState = value; }
		}

		/// <summary>
		/// Gets or sets a value that determines whether the network should use a random server when connecting.
		/// </summary>
		public bool UseRandomServer
		{
			get { return this.useRandomServer; }
			[Sync("setUseRandomServer")]
			set { this.useRandomServer = value; }
		}

		/// <summary>
		/// Gets or sets a list of commands to perform when connecting to a network.
		/// </summary>
		public string[] Perform
		{
			get { return this.perform; }
			[Sync("setPerform")]
			set { this.perform = value; }
		}

		/// <summary>
		/// Gets or sets a value that determines whether the client should be automatically identified upon connecting to a network.
		/// </summary>
		public bool UseAutoIdentify
		{
			get { return this.useAutoIdentify; }
			[Sync("setUseAutoIdentify")]
			set { this.useAutoIdentify = value; }
		}

		/// <summary>
		/// Gets or sets the name of the auto-identify service.
		/// </summary>
		public string AutoIdentifyService
		{
			get { return this.autoIdentifyService; }
			[Sync("setAutoIdentifyService")]
			set { this.autoIdentifyService = value; }
		}

		/// <summary>
		/// Gets or sets the password to use when idenfying the client.
		/// </summary>
		public string AutoIdentifyPassword
		{
			get { return this.autoIdentifyPassword; }
			[Sync("setAutoIdentifyPassword")]
			set { this.autoIdentifyPassword = value; }
		}

		/// <summary>
		/// Gets or sets a value that determines whether SASL should be used.
		/// </summary>
		public bool UseSasl
		{
			get { return this.useSasl; }
			[Sync("setUseSasl")]
			set { this.useSasl = value; }
		}

		/// <summary>
		/// Gets or sets the SASL account name.
		/// </summary>
		public string SaslAccount
		{
			get { return this.saslAccount; }
			[Sync("setSaslAccount")]
			set { this.saslAccount = value; }
		}

		/// <summary>
		/// Gets or sets the SASL password.
		/// </summary>
		public string SaslPassword
		{
			get { return this.saslPassword; }
			[Sync("setSaslPassword")]
			set { this.saslPassword = value; }
		}

		/// <summary>
		/// Gets or sets a value that determines whether the IRC client should automatically reconnect to the network when the connection is lost.
		/// </summary>
		public bool UseAutoReconnect
		{
			get { return this.useAutoReconnect; }
			[Sync("setUseAutoReconnect")]
			set { this.useAutoReconnect = value; }
		}

		/// <summary>
		/// Gets or sets the reconnect interval for auto-reconnect.
		/// </summary>
		public uint AutoReconnectInterval
		{
			get { return this.autoReconnectInterval; }
			[Sync("setAutoReconnectInterval")]
			set { this.autoReconnectInterval = value; }
		}

		/// <summary>
		/// Gets or sets the maximum number of retries for auto-reconnect.
		/// </summary>
		public ushort AutoReconnectRetries
		{
			get { return this.autoReconnectRetries; }
			[Sync("setAutoReconnectRetries")]
			set { this.autoReconnectRetries = value; }
		}

		/// <summary>
		/// Gets or sets a value that determines whether auto-reconnect may perform an unlimited number of connection retries.
		/// </summary>
		public bool UnlimitedReconnectRetries
		{
			get { return this.unlimitedReconnectRetries; }
			[Sync("setUnlimitedReconnectRetries")]
			set { this.unlimitedReconnectRetries = value; }
		}

		/// <summary>
		/// Gets or sets a value that determines whether all channels should be rejoined upon connecting to the network.
		/// </summary>
		public bool RejoinChannels 
		{
			get { return this.rejoinChannels; }
			[Sync("setRejoinChannels")]
			set { this.rejoinChannels = value; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Network.
		/// </summary>
		public Network(NetworkId id)
		{
			this.id = id;

			this.serverList = new List<Server>();
			this.supports = new Dictionary<string, string>();

			this.objects = new SyncableObjectCollection();
			this.users = new List<IrcUser>();
			this.channels = new List<IrcChannel>();
		}

		#endregion

		#region Method

		SyncableObject ISyncableObjectContainer.GetObject(string className, string objectName)
		{
			lock (this.objects)
				return this.objects.GetObject(className, objectName);
		}

		/// <summary>
		/// Tries to retreive the specified codec.
		/// </summary>
		/// <param name="codecName"></param>
		/// <returns></returns>
		private static Encoding getEncoding(byte[] codecName)
		{
			string codecNameStr = Encoding.UTF8.GetString(codecName);

			try
			{
				return Encoding.GetEncoding(codecNameStr);
			}
			catch (Exception ex)
			{
				Console.Error.WriteLine("Encoding \"{0}\" is not supported: {1}", codecNameStr, ex.Message);
				return null;
			}
		}

		/// <summary>
		/// Parses a network from a QVariantMap.
		/// </summary>
		/// <param name="data"></param>
		public void Parse(QVariantMap data)
		{
			this.NetworkName = data.Get<string>("networkName");
			this.CurrentServer = data.Get<string>("currentServer");
			this.MyNick = data.Get<string>("myNick");
			this.Latency = data.Get<int>("latency");
			this.setCodecForServer(data.Get<byte[]>("codecForServer"));
			this.setCodecForEncoding(data.Get<byte[]>("codecForEncoding"));
			this.setCodecForDecoding(data.Get<byte[]>("codecForDecoding"));
			this.IdentityId = (IdentityId)data.Get<int>("identityId");
			this.IsConnected = data.Get<bool>("isConnected");
			this.ConnectionState = (ConnectionState)data.Get<int>("connectionState");
			this.UseRandomServer = data.Get<bool>("useRandomServer");
			this.Perform = data.Get<string[]>("perform");
			this.UseAutoIdentify = data.Get<bool>("useAutoIdentify");
			this.AutoIdentifyService = data.Get<string>("autoIdentifyService");
			this.AutoIdentifyPassword = data.Get<string>("autoIdentifyPassword");
			this.UseSasl = data.Get<bool>("useSasl");
			this.SaslAccount = data.Get<string>("saslAccount");
			this.SaslPassword = data.Get<string>("saslPassword");
			this.UseAutoReconnect = data.Get<bool>("useAutoReconnect");
			this.AutoReconnectInterval = data.Get<uint>("autoReconnectInterval");
			this.AutoReconnectRetries = data.Get<ushort>("autoReconnectRetries");
			this.UnlimitedReconnectRetries = data.Get<bool>("unlimitedReconnectRetries");
			this.RejoinChannels = data.Get<bool>("rejoinChannels");

			this.setServerList(data.Get<QVariantList>("ServerList"));
			foreach (KeyValuePair<string, object> entry in data.Get<QVariantMap>("Supports"))
				this.addSupport(entry.Key, (string)entry.Value);

			QVariantMap usersAndChannels = data.Get<QVariantMap>("IrcUsersAndChannels");
			QVariantMap users = usersAndChannels.Get<QVariantMap>("Users");
			if (users != null)
			{
				int numUsers = (users.Get<QVariantList>("user")).Count;
				for (int i = 0; i < numUsers; i++)
				{
					IrcUser user = new IrcUser(this);
					user.User = (string)(users.Get<QVariantList>("user"))[i];
					user.Host = (string)(users.Get<QVariantList>("host"))[i];
					user.Nick = (string)(users.Get<QVariantList>("nick"))[i];
					user.RealName = (string)(users.Get<QVariantList>("realName"))[i];
					user.Away = (bool)(users.Get<QVariantList>("away"))[i];
					user.AwayMessage = (string)(users.Get<QVariantList>("awayMessage"))[i];
					user.IdleTime = (DateTime)(users.Get<QVariantList>("idleTime"))[i];
					user.LoginTime = (DateTime)(users.Get<QVariantList>("loginTime"))[i];
					user.Server = (string)(users.Get<QVariantList>("server"))[i];
					user.IrcOperator = (string)(users.Get<QVariantList>("ircOperator"))[i];
					user.LastAwayMessage = (int)(users.Get<QVariantList>("lastAwayMessage"))[i];
					user.WhoisServiceReply = (string)(users.Get<QVariantList>("whoisServiceReply"))[i];
					user.SuserHost = (string)(users.Get<QVariantList>("suserHost"))[i];
					user.Encrypted = (bool)(users.Get<QVariantList>("encrypted"))[i];
					user.UserModes = (string)(users.Get<QVariantList>("userModes"))[i];
					this.AddIrcUser(user);
				}
			}

			QVariantMap channels = usersAndChannels.Get<QVariantMap>("Channels");
			if (channels != null)
			{
				int numChannels = (channels.Get<QVariantList>("name")).Count;
				for (int i = 0; i < numChannels; i++)
				{
					IrcChannel channel = new IrcChannel(this);
					channel.Name = (string)(channels.Get<QVariantList>("name"))[i];
					channel.Topic = (string)(channels.Get<QVariantList>("topic"))[i];
					channel.Password = (string)(channels.Get<QVariantList>("password"))[i];
					channel.Encrypted = (bool)(channels.Get<QVariantList>("encrypted"))[i];
					
					this.AddIrcChannel(channel);
				}
			}

		}

		public override string ToString()
		{
			return this.networkName;
		}

		/// <summary>
		/// Gets the user with the specified name.
		/// </summary>
		/// <param name="fullName"></param>
		/// <returns></returns>
		public IrcUser GetUser(string fullName)
		{
			fullName = Irc.ToLowerCase(fullName);
			foreach (IrcUser user in this.users)
			{
				if (Irc.ToLowerCase(user.FullName) == fullName)
					return user;
			}

			return null;
		}

		/// <summary>
		/// Gets the user with the specified nick. The search is case-insensitive.
		/// </summary>
		/// <param name="nick"></param>
		/// <returns></returns>
		public IrcUser GetUserByNick(string nick)
		{
			nick = Irc.ToLowerCase(nick);
			foreach (IrcUser user in this.users)
			{
				if (Irc.ToLowerCase(user.Nick) == nick)
					return user;
			}

			return null;
		}

		/// <summary>
		/// Gets the channel with the specified name.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public IrcChannel GetChannel(string name)
		{
			foreach (IrcChannel channel  in this.channels)
			{
				if (channel.Name == name)
					return channel;
			}

			return null;
		}

		/// <summary>
		/// Gets the type of the specified channel mode.
		/// </summary>
		/// <param name="mode"></param>
		/// <returns></returns>
		public ChannelModeType GetChannelModeType(char mode)
		{
			if(mode == 0)
				return ChannelModeType.None;

			string chanModes;
			if (!this.supports.TryGetValue("CHANMODES", out chanModes))
				return ChannelModeType.None;

			ChannelModeType modeType = ChannelModeType.A;

			for (int i = 0; i < chanModes.Length; i++)
			{
				if (chanModes[i] == mode)
					break;
				if (chanModes[i] == ',')
					modeType = (ChannelModeType)((int)(modeType) << 1);
			}

			if (modeType > ChannelModeType.D)
			{
				Console.Error.WriteLine("Invalid CHANMODES: \"{0}\"", chanModes);
				return ChannelModeType.None;
			}

			return modeType;
		}

		#region Misc

		[Sync("setCodecForServer")]
		private void setCodecForServer(byte[] codecName)
		{
			this.codecForServer = codecName == null ? null : getEncoding(codecName);
		}

		[Sync("setCodecForEncoding")]
		private void setCodecForEncoding(byte[] codecName)
		{
			this.codecForEncoding = codecName == null ? null : getEncoding(codecName);
		}

		[Sync("setCodecForDecoding")]
		private void setCodecForDecoding(byte[] codecName)
		{
			this.codecForDecoding = codecName == null ? null : getEncoding(codecName);
		}

		[Sync("setServerList")]
		private void setServerList(QVariantList list)
		{
			this.serverList.Clear();
			foreach (Server server in list)
				this.serverList.Add(server);
		}

		[Sync("addSupport")]
		private void addSupport(string param, string value)
		{
			this.supports[param] = value;
		}

		[Sync("removeSupport")]
		private void removeSupport(string param)
		{
			this.supports.Remove(param);
		}

		#endregion

		#region IrcUser

		public void AddIrcUser(IrcUser user)
		{
			this.users.Add(user);

			lock (this.objects)
				this.objects.Add(new SyncableObject("IrcUser", user.Nick, user));
		}

		[Sync("addIrcUser")]
		private void addIrcUser(string fullName)
		{
			this.AddIrcUser(new IrcUser(this, fullName));
		}

		public void RemoveIrcUser(IrcUser user)
		{
			if (this.users.Remove(user))
			{
				SyncableObject userObj;

				lock (this.objects)
					userObj = this.objects.GetObject("IrcUser", user.Nick);
				
				if (userObj != null)
				{
					lock (this.objects)
						this.objects.Remove(userObj);
				}
			}
		}

		#endregion

		#region IrcChannel
	
		public void AddIrcChannel(IrcChannel channel)
		{
			this.channels.Add(channel);
			lock (this.objects)
				this.objects.Add(new SyncableObject("IrcChannel", channel.Name, channel));
		}

		[Sync("addIrcChannel")]
		private void addIrcChannel(string channelName)
		{
			this.AddIrcChannel(new IrcChannel(this, channelName));
		}

		public void RemoveIrcChannel(IrcChannel channel)
		{
			if (this.channels.Remove(channel))
			{
				SyncableObject channelObj;

				lock (this.objects)
					channelObj = this.objects.GetObject("IrcChannel", channel.Name);
				
				if (channelObj != null)
				{
					lock (this.objects)
						this.objects.Remove(channelObj);
				}
			}
		}

		#endregion

		#region Events

		internal void OnStatusMessage(StatusMessageEventArgs e)
		{
			if (this.StatusMessage != null)
				this.StatusMessage(this, e);
		}

		#endregion

		#endregion

	}
}
