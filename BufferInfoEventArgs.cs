﻿using System;

namespace QuasselSharp
{
	/// <summary>
	/// BufferInfo event args.
	/// </summary>
	public sealed class BufferInfoEventArgs : EventArgs
	{
		#region Fields

		private readonly BufferInfo buffer;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the buffer info.
		/// </summary>
		public BufferInfo Buffer
		{
			get { return this.buffer; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BufferInfoEventArgs.
		/// </summary>
		public BufferInfoEventArgs(BufferInfo buffer)
		{
			this.buffer = buffer;
		}

		#endregion

		#region Methods

		#endregion
	}
}
