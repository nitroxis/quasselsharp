﻿using System;
using System.Globalization;
using QuasselSharp.Protocol;

namespace QuasselSharp
{
	/// <summary>
	/// Represents a Quassel peer.
	/// </summary>
	public abstract class Peer
	{
		#region Fields

		private AuthHandler authHandler;
		private SignalProxy signalProxy;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the peer's auth handler.
		/// </summary>
		public AuthHandler AuthHandler
		{
			get { return this.authHandler; }
			set { this.authHandler = value; }
		}

		/// <summary>
		/// Gets or sets the peer's signal proxy.
		/// </summary>
		public SignalProxy SignalProxy
		{
			get { return this.signalProxy; }
			set { this.signalProxy = value; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new RemotePeer.
		/// </summary>
		protected Peer()
		{

		}

		#endregion

		#region Methods

		public abstract void Dispatch(RegisterClient message);
		public abstract void Dispatch(ClientDenied message);
		public abstract void Dispatch(ClientRegistered message);
		public abstract void Dispatch(SetupData message);
		public abstract void Dispatch(SetupFailed message);
		public abstract void Dispatch(SetupDone message);
		public abstract void Dispatch(Login message);
		public abstract void Dispatch(LoginFailed message);
		public abstract void Dispatch(LoginSuccess message);
		public abstract void Dispatch(SessionState message);
		public abstract void Dispatch(SyncMessage message);
		public abstract void Dispatch(RpcCall message);
		public abstract void Dispatch(InitRequest message);
		public abstract void Dispatch(InitData message);

		/// <summary>
		/// Dispatches an init request for the specified network.
		/// </summary>
		/// <param name="network">The network ID.</param>
		public void DispatchInitNetwork(NetworkId network)
		{
			this.Dispatch(new InitRequest("Network", ((int)network).ToString(CultureInfo.InvariantCulture)));
		}

		/// <summary>
		/// Handles a message.
		/// </summary>
		/// <param name="message"></param>
		public virtual void Handle(PeerMessage message)
		{
			if (message == null)
				throw new ArgumentNullException("message");

			if (message is SignalProxyMessage)
			{
				if (this.signalProxy == null)
					throw new Exception("Cannot handle message without a SignalProxy!");

				this.signalProxy.Handle((SignalProxyMessage)message);
			}
			else if (message is HandshakeMessage)
			{
				if (this.authHandler == null)
					throw new Exception("Cannot handle auth messages without an active AuthHandler!");

				this.authHandler.Handle((HandshakeMessage)message);
			}
			else
			{
				throw new NotSupportedException(string.Format("Message \"{0}\" is not supported.", message.GetType().Name));
			}
		}

		#endregion
	}
}
