﻿using System;
using System.Text;

namespace QuasselSharp
{
	/// <summary>
	/// IRC helper.
	/// </summary>
	public static class Irc
	{
		#region Constants

		public const string LowerCaseChars = @"abcdefghijklmnopqrstuvwxyz{}|";
		public const string UpperCaseChars = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ[]\";

		#endregion

		#region Methods

		/// <summary>
		/// Returns a value that indicates whether the specified character is a letter per IRC specification.
		/// </summary>
		/// <param name="c"></param>
		/// <returns></returns>
		public static bool IsLetter(char c)
		{
			return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
		}

		/// <summary>
		/// Returns a value that indicates whether the specified character is a digit per IRC specification.
		/// </summary>
		/// <param name="c"></param>
		/// <returns></returns>
		public static bool IsDigit(char c)
		{
			return (c >= '0' && c <= '9');
		}

		/// <summary>
		/// Returns a value that indicates whether the specified character is a special character per IRC specification.
		/// </summary>
		/// <param name="c"></param>
		/// <returns></returns>
		public static bool IsSpecial(char c)
		{
			return c == '[' || c == ']' || c == '\\' || c == '`' || c == '_' || c == '^' || c == '{' || c == '|' || c == '}';
		}

		/// <summary>
		/// Returns a value that indicates whether the specified string is a valid nick per IRC specification.
		/// </summary>
		/// <param name="nick"></param>
		/// <returns></returns>
		public static bool IsValidNick(string nick)
		{
			if (string.IsNullOrEmpty(nick))
				return false;

			if (!IsValidNickStart(nick[0]))
				return false;

			for (int i = 1; i < nick.Length; i++)
			{
				if (!IsValidChar(nick[i]))
					return false;
			}

			return true;
		}

		/// <summary>
		/// Returns a value that indicates whether the specified character is valid per IRC specification.
		/// </summary>
		/// <param name="c"></param>
		/// <returns></returns>
		public static bool IsValidChar(char c)
		{
			return IsLetter(c) || IsDigit(c) || IsSpecial(c) || c == '-';
		}

		/// <summary>
		/// Returns a value that indicates whether the specified character is a valid nick start character per IRC specification.
		/// </summary>
		/// <param name="c"></param>
		/// <returns></returns>
		public static bool IsValidNickStart(char c)
		{
			return IsLetter(c) || IsSpecial(c);
		}

		/// <summary>
		/// Returns the specified string in lower case per IRC specification.
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static string ToLowerCase(string str)
		{
			if (str == null)
				return null;

			StringBuilder lower = new StringBuilder(str.Length);
			for (int i = 0; i < str.Length; i++)
			{
				int index = UpperCaseChars.IndexOf(str, StringComparison.Ordinal);
				if (index >= 0)
					lower.Append(LowerCaseChars[index]);
				else
					lower.Append(str[i]);
			}

			return lower.ToString();
		}

		/// <summary>
		/// Returns the specified string in upper case per IRC specification.
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static string ToUpperCase(string str)
		{
			if (str == null)
				return null;

			StringBuilder upper = new StringBuilder(str.Length);
			for (int i = 0; i < str.Length; i++)
			{
				int index = LowerCaseChars.IndexOf(str, StringComparison.Ordinal);
				if (index >= 0)
					upper.Append(UpperCaseChars[index]);
				else
					upper.Append(str[i]);
			}

			return upper.ToString();
		}

		#endregion
	}
}
