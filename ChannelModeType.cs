﻿using System;

namespace QuasselSharp
{
	/// <summary>
	/// Defines the channel mode types.
	/// </summary>
	[Flags]
	public enum ChannelModeType
	{
		None,
		A = 1,
		B = 2,
		C = 4,
		D = 8,
	}
}
